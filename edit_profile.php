<?php
session_start();
if ($_SESSION['m_id'] == "") {
    echo "Please Login!";
    exit();
}
$m_id = $_SESSION['m_id'];
include "../../config.php";

include "header.php";
include "nav-bar_nofix.php";
?>

<?php
$sql = "SELECT * FROM members WHERE m_id = $m_id  ";
$query = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($query)) {
    ?>



<!-- Edit -->
<div id="edit<?php echo $row['m_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

				<?php
$edit = mysqli_query($conn, "select * from members where m_id='" . $row['m_id'] . "'");
    $erow = mysqli_fetch_array($edit);
    ?>
<div class="container">

<div class="jumbotron mt-4">
<h2 class="text-center">ข้อมูลส่วนตัว</h2>
<hr class="my-3">

<form method="POST" action="process.php?cmd=edit&m_id=<?php echo $row['m_id']; ?>"  >
<div class="form-row">
    <div class="form-group col-md-6">
      <label>ชื่อ</label>
      <input type="text" class="form-control"  placeholder="กรอกชื่อ" name="m_firstname"  value="<?php echo $erow['m_firstname']; ?>" required>
    </div>
    <div class="form-group col-md-6">
      <label >นามสกุล</label>
      <input type="text" class="form-control"  placeholder="กรอกนามสกุล" name="m_lastname"  value="<?php echo $erow['m_lastname']; ?>" required>
    </div>



  </div>
<div class="form-row">
  <div class="col-md-2">

      <label class="mr-sm-2" for="inlineFormCustomSelect">เพศ</label>
      <select class="custom-select mr-sm-2" name="m_gender" value="<?php echo $erow['m_gender']; ?>">
        <option selected>เลือก : <?php echo $erow['m_gender']; ?></option>
        <option value="ชาย"   checked>ชาย</option>
        <option value="หญิง"  checked>หญิง</option>
      </select>
      </div>

      <div class="form-group col-md-5">
      <label >วันเกิด</label>
      <input type="date" class="form-control"  placeholder="กรอกนามสกุล" name="m_birth_date" value="<?php echo $erow['m_birth_date']; ?>"  required>
    </div>

    <div class="form-group col-md-5">
      <label >อีเมล</label>
      <input type="email" class="form-control"  placeholder="name@example.com" name="m_email" value="<?php echo $erow['m_email']; ?>" required>
    </div>
  </div>



  <hr class="my-4">

  <div class="form-group">
    <label for="inputAddress">คำถามกันลืม</label>
    <textarea class="form-control" name="m_forget_ans"  cols="30" rows="3"  required placeholder="กรอกคำถามกันลืมรหัสผ่าน"><?php echo $erow['m_forget_ans']; ?></textarea>
  </div>

  <div class="form-group">
    <label for="inputAddress2">คำตอบ</label>
    <input type="text" class="form-control"  name="m_ans" value="<?php echo $erow['m_ans']; ?>" placeholder="ใส่คำตอบ">
  </div>
  <button class="btn btn-lg btn btn-warning btn-block text-white" type="submit">แก้ไข</button>

</form>
</div>



</div>

<?php
}
?>


<?php include 'footer.php'?>
</body>
</html>
<?php
mysqli_close($conn);
?>