<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    

    <title>Cover Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../public/stylesheets/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="../public/stylesheets/home.css" rel="stylesheet">
  </head>

  <body>


  <header style="background: linear-gradient(88deg, #fb9678, #ffb463);">
  <div class="navbar navbar-expand-lg  ">
  <a class="navbar-brand" href="home.php"><strong>SCT</strong>  Management System</a>

  <div class=" navbar-collapse flex-row-reverse " id="navbarSupportedContent">
    <ul class="navbar-nav ">
      <li class="nav-item ">
        <a class="nav-link" href="login.html">เข้าสู่ระบบ </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="register.php">สมัครสมาชิก</a>
      </li>
    </ul>
  </div>
</div>
</header>

<div class="container text-center ">
  <img src="sct.jpg"  alt="">
</div>


    
    <script src="../public/javascripts/jquery.min.js"></script>
    <script src="../public/javascripts/popper.min.js"></script>
    <script src="../public/javascripts/bootstrap.min.js"></script>

  </body>
</html>
