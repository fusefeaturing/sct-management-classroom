<?php include 'Model/config.php'?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
	<link rel="stylesheet" href="../public/node_modules/mdi/css/materialdesignicons.min.css" />
  <link rel="stylesheet" href="../public/node_modules/simple-line-icons/css/simple-line-icons.css" />
  <link rel="stylesheet" href="../public/node_modules/flag-icon-css/css/flag-icon.min.css" />
	<link rel="stylesheet" href="../public/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" />
  <link rel="stylesheet" href="../public/css/style.css" /> 
	<link rel="shortcut icon" href="../public/img/favicon.png" />

	<link rel="stylesheet" href="../public/node_modules/dropify/dist/css/dropify.min.css" />
	<link rel="stylesheet" href="../public/node_modules/jquery-file-upload/css/uploadfile.css" />

	
	

</head>

<body>

	<div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
      <div class="row">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth register-full-bg">
          <div class="row w-100">
            <div class="mx-auto">
              <div class="auth-form-dark text-left p-5">
                <h2>สมัครสมาชิก</h2>
                
                <form class="pt-4" method="POST" action="Model/process.php?cmd=register"  enctype="multipart/form-data">
								<div class="row">
								<div class="col-md-6">
                   <div class="form-group row">
                    <label >รหัสผู้ใช้งาน</label>
                    <input type="text" name="username" class="form-control"  placeholder="" required/>
                    
									</div>
									</div>
									<div class="col-md-6">
                  <div class="form-group row">
                    <label >รหัสผ่าน</label>
                    <input type="password" name="password" class="form-control"  placeholder="" required/>
                    
									</div>
									</div>
								</div>
								<div class="row">
								<div class="col-md-6">
                   <div class="form-group row">
                    <label >ชื่อ</label>
                    <input type="text" name="firstname" class="form-control"  placeholder="" required/>
                    
									</div>
									</div>
									<div class="col-md-6">
                  <div class="form-group row">
                    <label >นามสกุล</label>
                    <input type="text" name="lastname" class="form-control"  placeholder="" required/>
                    
									</div>
									</div>
								</div>
								<div class="row">
								<?php
$sql1 = "select * from faculty";
$query1 = mysqli_query($conn, $sql1);
$sql2 = "select * from branch";
$query2 = mysqli_query($conn, $sql2);
?>
								<div class="col-md-6">
                   <div class="form-group row">
                    <label >คณะ</label>
                    <select name="faculty" class="form-control" >
									<option style="background: rgba(0, 0, 0, 0.6); "required>--------------เลือกคณะ--------------</option>
									<?php while ($row1 = mysqli_fetch_array($query1)): ;?>

																<option style="background: rgba(0, 0, 0, 0.6);" value="<?php echo $row1['faculty_id'] ?>"><?php echo $row1['faculty_name'] ?></option>
															                <?php endwhile;?>
							</select>
                    
									</div>
									</div>
									<div class="col-md-6">
                  <div class="form-group row">
                    <label >สาขา</label>
                    <select name="branch" class="form-control">
									<option style="background: rgba(0, 0, 0, 0.6);" required>---------------เลือกสาขา--------------</option>
									<?php while ($row1 = mysqli_fetch_array($query2)): ;?>

															<option style="background: rgba(0, 0, 0, 0.6);" value="<?php echo $row1['branch_id'] ?>"><?php echo $row1['branch_name'] ?></option>
														                <?php endwhile;?>
							</select>
                    
									</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
                	   <div class="form-group row">
                	    <label >เบอร์โทรศัพท์</label>
                	    <input type="text" name="tel" class="form-control"  placeholder="" required/>
	
										</div>
									</div>
										<div class="col-md-6">
                	  	<div class="form-group row">
                	  	  <label ></label>
                	  	  <input type="hidden" name="member_type" class="form-control" value="student">

											</div>
										</div>
								</div>
								
							
								<div class="row">
        				  <div class="col-lg-6 grid-margin stretch-card">
        				    <div class="card">
        				      <div class="card-body">
        				        <h4 class="card-title">เลือกรูปโปรไฟล์</h4>
        				        <input type="file" name="image" class="dropify" data-max-file-size="30kb" required/>
        				      </div>
        				    </div>
        				  </div>        				 
								</div>
								
                  <div class=" text-center">
									<button type="submit" class="btn btn-warning btn-lg font-weight-medium col-4">บันทึก</a>                   
                  </div>
                  <div class="mt-2 text-center">
                    <a href="login.html" class="auth-link text-light">คุณมีบัญชีอยู่แล้ว? <span class="font-weight-medium">ลงชื่อเข้าสู่ระบบ</span></a>
                  </div>
								</form>
								
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
  </div>        

	<script src="../public/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="../public/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="../public/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../public/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>  
  <script src="../public/js/off-canvas.js"></script>
  <script src="../public/js/hoverable-collapse.js"></script>
  <script src="../public/js/misc.js"></script>
  <script src="../public/js/settings.js"></script>
	<script src="../public/js/todolist.js"></script>

 
  <script src="../public/node_modules/dropify/dist/js/dropify.min.js"></script>
 
  <script src="../public/js/dropify.js"></script>


	
</body>

</html>