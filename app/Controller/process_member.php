<?php
session_start();
include '../Model/config.php';
$upload_dir = '../../public/images/'; //ใช้เรียกภาพใน folder uploads
error_reporting(~E_NOTICE);

//add
if ($_GET['cmd'] == "add") {

    $username = $_POST['username'];
    $password = $_POST['password'];

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $faculty = $_POST['faculty'];
    $branch = $_POST['branch'];
    $member_type = $_POST['member_type'];
    $tel = $_POST['tel'];
//image
    $imgName = $_FILES['image']['name'];
    $imgTmp = $_FILES['image']['tmp_name'];
    $imgSize = $_FILES['image']['size'];

    //echo $imgName . "<br>";
    //echo $imgTmp . "<br>";
    //echo $imgSize . "<br>";

    $imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

    $allowExt = array('jpeg', 'jpg', 'png', 'gif');

    $userPic = time() . '_' . rand(1000, 9999) . '.' . $imgExt;

    if (in_array($imgExt, $allowExt)) {

        if ($imgSize < 5000000) {
            move_uploaded_file($imgTmp, $upload_dir . $userPic);
        } else {
            $errorMsg = 'Image too large';
        }
    } else {
        $errorMsg = 'Please select a valid image';
    }

    //echo $userPic;

    $sql = "insert into member (faculty_id, branch_id, member_username, member_password, member_profile_pic,
    member_firstname ,member_lastname, member_tel ,member_type)
    values ('$faculty','$branch','$username','$password' ,'$userPic','$firstname','$lastname','$tel','$member_type')";
    if (mysqli_query($conn, $sql)) {
        echo "<script>
            alert('เพิ่มข้อมูลเรียบร้อย');
            window.location = '../View/member.php';
            </script>";
        
    } else {
        echo "Error updating record: " . mysqli_error($conn);
    }

}

//delete
if ($_GET['cmd'] == "delete") {

    $id = $_GET['id'];
    mysqli_query($conn, "delete from member where member_id='$id'");
    echo "<script>
            alert('ลบข้อมูลเรียบร้อย');
            window.location = '../View/member.php';
            </script>";
    

}

//edit
if ($_GET['cmd'] == "edit") {

    $id = $_GET['id'];

    $username = $_POST['username'];
    $password = $_POST['password'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $faculty = $_POST['faculty'];
    $branch = $_POST['branch'];
    $tel = $_POST['tel'];
    $member_type = $_POST['member_type'];
//image
    $imgName = $_FILES['image']['name'];
    $imgTmp = $_FILES['image']['tmp_name'];
    $imgSize = $_FILES['image']['size'];

        $imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

        $allowExt = array('jpeg', 'jpg', 'png', 'gif');

        $userPic = time() . '_' . rand(1000, 9999) . '.' . $imgExt;        
 
        if ($imgName == "" || $imgTmp == "" || $imgSize == "") {
        
           
            if ($_SESSION["member_type"] == "admin") {

                $sql = "update member set faculty_id='$faculty', branch_id='$branch' ,member_username='$username',member_password='$password'
            , member_firstname='$firstname', member_lastname='$lastname'
            ,member_tel='$tel', member_type='$member_type' where member_id='$id' ";
        
                $query = mysqli_query($conn, $sql);
                if ($query) {   
        
                    echo "<script>
                    alert('แก้ไขข้อมูลเรียบร้อย');
                    window.location = '../View/member.php';
                    </script>";
        
                } 
        
            } else if ($_SESSION["member_type"] == "professor") {
                $sql = "update member set faculty_id='$faculty', branch_id='$branch' ,member_password='$password'
                , member_firstname='$firstname', member_lastname='$lastname'
                ,member_tel='$tel' where member_id='$id' ";
            
                    $query = mysqli_query($conn, $sql);
                    if ($query) {
            
                        echo "<script>
                        alert('แก้ไขข้อมูลเรียบร้อย');
                        window.location = '../View/edit_profile.php';
                        </script>";
            
                    } 
        
            } else if ($_SESSION["member_type"] == "student") {
                 $sql = "update member set faculty_id='$faculty', branch_id='$branch' ,member_password='$password'
                , member_firstname='$firstname', member_lastname='$lastname'
                ,member_tel='$tel' where member_id='$id' ";
            
                    $query = mysqli_query($conn, $sql);
                    if ($query) {
                        echo "<script>
                        alert('แก้ไขข้อมูลเรียบร้อย');
                        window.location = '../View/edit_profile.php';
                        </script>";
                    }        
            
                 } else {
                         echo "Error updating record: " . mysqli_error($conn);
                     }   
            
            
            } else {
                 if (in_array($imgExt, $allowExt)) {

                             if ($imgSize < 5000000) {
                               /* unlink($upload_dir . $row['image']);*/
                                move_uploaded_file($imgTmp, $upload_dir . $userPic);
                                 } else {
                                     $errorMsg = 'Image too large';
                                 }

                         } else {
                             $errorMsg = 'Please select a valid image';
                         }
                    
             if ($_SESSION["member_type"] == "admin") {

        $sql = "update member set faculty_id='$faculty', branch_id='$branch' ,member_username='$username',member_password='$password'
    , member_lastname='$lastname' ,member_profile_pic='$userPic', member_firstname='$firstname', member_lastname='$lastname'
    ,member_tel='$tel', member_type='$member_type' where member_id='$id' ";

        $query = mysqli_query($conn, $sql);
        if ($query) { 

            echo "<script>
            alert('แก้ไขข้อมูลเรียบร้อย');
            window.location = '../View/member.php';
            </script>";

        } 

    } else if ($_SESSION["member_type"] == "professor") {
        $sql = "update member set faculty_id='$faculty', branch_id='$branch' ,member_password='$password'
        , member_lastname='$lastname' ,member_profile_pic='$userPic', member_firstname='$firstname', member_lastname='$lastname'
        ,member_tel='$tel' where member_id='$id' ";
    
            $query = mysqli_query($conn, $sql);
            if ($query) {
    
                echo "<script>
                alert('แก้ไขข้อมูลเรียบร้อย');
                window.location = '../View/edit_profile.php';
                </script>";
    
            } 

    } else if ($_SESSION["member_type"] == "student") {
         $sql = "update member set faculty_id='$faculty', branch_id='$branch' ,member_password='$password'
        , member_lastname='$lastname' ,member_profile_pic='$userPic', member_firstname='$firstname', member_lastname='$lastname'
        ,member_tel='$tel' where member_id='$id' ";
    
            $query = mysqli_query($conn, $sql);
            if ($query) {
                echo "<script>
                alert('แก้ไขข้อมูลเรียบร้อย');
                window.location = '../View/edit_profile.php';
                </script>";
            }        
    
    } else {
            echo "Error updating record: " . mysqli_error($conn);
        }       
                
            }

       
  


   
    
}
