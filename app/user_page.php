<?php

session_start();
/*if ($_SESSION['member_id'] == "") {
    echo "Please Login!";
    exit();
}

if ($_SESSION['member_type'] != "student") {
    echo "This page for user only!";
    exit();
}*/

include '../../Model/config.php';

$strSQL = "SELECT m.member_id , m.member_firstname , b.branch_name  FROM member AS m  
            left join branch AS b ON (m.branch_id = b.branch_id)
            WHERE m.member_id='" . $_SESSION["member_id"] . "'";
$objQuery = mysqli_query($conn, $strSQL);
$objResult = mysqli_fetch_array($objQuery, MYSQLI_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="../../../public/stylesheets/bootstrap.min.css">
  <link rel="stylesheet" href="../../../public/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="../../../public/stylesheets/style.css">


</head>
<body>

<header class="navbar navbar-expand navbar-dark bg-warning">
        <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>
        <a class="navbar-brand" href="#"><i class="fa fa-code-branch"></i> SCT</a>
    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                    <i class="fa fa-user"></i> <?php echo $_SESSION["member_firstname"] ?>
				</a>
			    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Action</a>
					<a class="dropdown-item" href="../../Model/process.php?cmd=logout">ออกจากระบบ</a>

                </div>
            </li>
        </ul>
    </div>
</header>

<div class="d-flex">
    <nav class="sidebar bg-dark">
	<li class="header">MAIN NAVIGATION</li>
        <ul class="list-unstyled">
			<li><a href="edit_profile.php"><i class="fas fa-user-edit"></i> แก้ไขข้อมูลผู้ใช้</a></li>
            <li><a href="class_schedule.php"><i class="fas fa-table"></i> จองห้องเรียน</a></li>
            <li><a href="cancel_booking.php"><i class="fas fa-ban"></i> ยกเลิกการจอง</a></li


        </ul>
    </nav>

    <div class="content p-4">

		<div class="container">

  ยินดีต้อนรับ <br>
  <table border="1" style="width: 300px">
    <tbody>
      <tr>
        <td width="87">  &nbsp;ชื่อ</td>
        <td width="197"><?php echo $objResult["member_firstname"]; ?>
        </td>
      </tr>
      <tr>
        <td> &nbsp;สาขา</td>
        <td><?php echo $objResult["branch_name"]; ?></td>
      </tr>
    </tbody>
  </table>


</div>
</div>
</div>

<script src="../../../public/javascripts/jquery.min.js"></script>
<script src="../../../public/javascripts/popper.min.js"></script>
<script src="../../../public/javascripts/bootstrap.min.js"></script>

</body>
</html>




<?php
mysqli_close($conn);



/*"SELECT
    cr.*
FROM
    classroom AS cr
WHERE
    cr.classroom_name NOT IN
    
    (
    SELECT
        rq.classroom_id
    FROM
        request_classroom AS rq
    WHERE
        rq.rq_approve_status = '0' AND(
            rq.rq_day_start BETWEEN "2018-10-12" AND "2018-10-12"
        ) AND(
            rq.rq_day_end >= "2018-10-12" AND "2018-10-12"
        ) AND(
            rq.rq_time_start <= "14:00:00" AND "17:00:00"
        ) AND(
            rq.rq_time_end > "14:00:00" AND "17:00:00"
        ) 
        &(
        SELECT
            sc.schedule_name
        FROM SCHEDULE AS
            sc
        WHERE
            sc.schedule_day2 = '5' AND(
                sc.schedule_time_start <= "14:00:00" AND "17:00:00"
            ) AND(
                sc.schedule_time_end > "14:00:00" AND "17:00:00"
            )
    )
)"*/

?>