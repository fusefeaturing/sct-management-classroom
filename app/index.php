<?php
session_start();

?>






<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../public/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" href="../public/fontawesome/css/all.min.css">

    <link rel="stylesheet" href="../public/stylesheets/sheets.css">
</head>
<body>
<div class="wrapper">

        <header class="navbar navbar-expand navbar-dark bg-warning">
                <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>
                <a class="navbar-brand" href="#"><i class="fa fa-code-branch"></i> SCT</a>
            <div class="navbar-collapse collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                            <i class="fa fa-user"></i> <?php echo $_SESSION["member_firstname"] ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="Model/process.php?cmd=logout">ออกจากระบบ</a>

                        </div>
                    </li>
                </ul>
            </div>
        </header>

        <div class="d-flex">
            <nav class="sidebar bg-dark">
                <li class="header"></li>
                    <ul class="list-unstyled">
                        <?php   /*
                            if ( $_SESSION["member_type"] == "admin") {
                                echo "<li><a href='View/member.php'><i class='fas fa-user'></i> ข้อมูลผู้ใช้งาน</a></li>
                                <li><a href='View/faculty.php'><i class='fas fa-clipboard'></i> คณะ</a></li>
                                <li><a href='View/branch.php'><i class='fas fa-clipboard'></i> สาขา</a></li>
                                <li><a href='View/building.php'><i class='fas fa-building'></i> ข้อมูลอาคารสถานที่</a></li>
                                <li><a href='View/classroom.php'><i class='fas fa-door-open'></i> ข้อมูลห้องเรียน</a></li>
                                <li><a href='View/equipment.php'><i class='fas fa-cog'></i> อุปกรณ์</a></li>
                                <li><a href='View/table.php'><i class='fas fa-cube'></i> ลักษณะโต๊ะ</a></li>
                                <li><a href='View/chair.php'><i class='fas fa-couch'></i> ลักษณะเก้าอี้</a></li>
                                <li><a href='View/class_schedule.php'><i class='fas fa-table'></i> จองห้องเรียน</a></li>
                                <li><a href='View/approve.php'><i class='fas fa-thumbs-up'></i> อนุมัติการจอง</a></li>
                                <li><a href='View/bookinglist.php'><i class='fas fa-clipboard-list'></i> รายการจอง</a></li>";
                            } else if ( $_SESSION["member_type"] == "professor") {
                                echo "
                                <li><a href='View/edit_profile.php'><i class='fas fa-user-edit'></i> แก้ไขข้อมูลผู้ใช้</a></li>
                                <li><a href='View/class_schedule.php'><i class='fas fa-table'></i> จองห้องเรียน</a></li>
                                <li><a href='View/cancel_booking.php'><i class='fas fa-ban'></i> ยกเลิกการจอง</a></li>
                                <li><a href='View/bookinglist.php'><i class='fas fa-clipboard-list'></i> รายการจอง</a></li>";
                            } else if ( $_SESSION["member_type"] == "student") {
                                echo "
                                <li><a href='View/edit_profile.php'><i class='fas fa-user-edit'></i> แก้ไขข้อมูลผู้ใช้</a></li>
                                <li><a href='View/class_schedule.php'><i class='fas fa-table'></i> จองห้องเรียน</a></li>
                                <li><a href='View/cancel_booking.php'><i class='fas fa-ban'></i> ยกเลิกการจอง</a></li>
                                <li><a href='View/bookinglist.php'><i class='fas fa-clipboard-list'></i> รายการจอง</a></li>";
                            } else {
                                echo "Error Session Menu";
                            }
                        
                       */ ?>

                    </ul>
                </nav>

        <div class="content p-4">

<div class="container">

<div class="container-fluid">

                                                <?php

if ( $_SESSION["member_type"] == "admin") {
    
    header('location:View/approve.php');
    
   
} else if ( $_SESSION["member_type"] == "professor") {
    
    header('location:View/bookinglist.php');
   
   
} else if ( $_SESSION["member_type"] == "student") {
    header('location:View/bookinglist.php');
   
} else {
    echo "Error Session content";
}

                                                ?>


</div>

</div>

</div>

</div>








</div>
<script src="../public/javascripts/jquery.min.js"></script>
<script src="../public/javascripts/popper.min.js"></script>
<script src="../public/javascripts/bootstrap.min.js"></script>
<script src="../public/javascripts/bsadmin.js"></script>
</body>
</html>
