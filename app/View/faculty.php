<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
</head>
<body>
<?php
    include '../Model/config.php';
    
?>
<div class="container-scroller">
  
  <?php
          include ("testheader.php")
      ?>

  <div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
     
   
      <?php
          include ("menu.php")
      ?>
   
      <div class="content-wrapper">

      
      <div class="card">
            <div class="card-body">
            <span class="pull-right"><a href="#addnew" data-toggle="modal" class="btn btn-outline-primary" ><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูลคณะ</a></span>
              <h4 class="card-title">คณะ</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
                            <th>ชื่อคณะ</th>
				            <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
$sql = "SELECT * FROM faculty   ORDER BY faculty_id DESC";

$query = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($query)) {
    ?>
					<tr>
						<td><?php echo $row['faculty_name']; ?></td>

						<td>
						<div align="center"><a href="#read<?php echo $row['faculty_id']; ?>" data-toggle="modal" class="btn btn-info" ><span class="glyphicon glyphicon-edit"></span> ดู</a> &nbsp;
								<a href="#edit<?php echo $row['faculty_id']; ?>" data-toggle="modal" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> แก้ไข</a> &nbsp;
								<a href="#del<?php echo $row['faculty_id']; ?>" data-toggle="modal" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> </div>

    <!-- read -->
    <div class="modal fade" id="read<?php echo $row['faculty_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">คณะ</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$data = mysqli_query($conn, "select * from faculty where faculty_id='" . $row['faculty_id'] . "'");
    $drow = mysqli_fetch_array($data);
    /*$upload_dir = '../../public/images/';*/?>
				<div class="container-fluid">
                    <!--<img src="<?php echo $upload_dir . $drow['image'] ?>" height="200">-->
					
					<h5>คณะ : <strong><?php echo $drow['faculty_name']; ?></strong></h5>

				</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

	<!-- Delete -->
    <div class="modal fade" id="del<?php echo $row['faculty_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ลบ</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$del = mysqli_query($conn, "select * from faculty where faculty_id='" . $row['faculty_id'] . "'");
    $drow = mysqli_fetch_array($del);?>
				<div class="container-fluid">
					<h5 align="center">คณะ : <strong><?php echo $drow['faculty_name']; ?></strong></h5>
                </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <a href="../Controller/process_faculty.php?cmd=delete&id=<?php echo $row['faculty_id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> <!-- form -->
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

<!-- Edit -->
    <div class="modal fade" id="edit<?php echo $row['faculty_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">แก้ไข</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$edit = mysqli_query($conn, "select * from faculty where faculty_id='" . $row['faculty_id'] . "'");
    $erow = mysqli_fetch_array($edit);?>
				<div class="container-fluid">
					<form class="form-modal" method="POST" action="../Controller/process_faculty.php?cmd=edit&id=<?php echo $erow['faculty_id']; ?>" enctype="multipart/form-data"> <!-- form -->
                    
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">คณะ</label>
                          <div class="col-sm-9">
                            <input text="text" name="faculty_name" class="form-control" value="<?php echo $erow['faculty_name']; ?>"/>
                          </div>
                        </div>
                      </div>                      
                    </div>                    
                    

               	 </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> บันทึก</button>
                </div>
				</form>
            </div>
        </div>
    </div>
<!-- /.modal -->

						</td>
					</tr>
					<?php
}

?>
            </tbody>
                    </table>
                    <!-- Add New -->
    <div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ระบุชื่อคณะที่ต้องการเพิ่ม</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<div class="container-fluid">
				<!-- form -->
				<form class="form-modal" method="POST" action="../Controller/process_faculty.php?cmd=add"  enctype="multipart/form-data">
                    
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">คณะ</label>
                          <div class="col-sm-9">
                            <input text="text" name="faculty_name" class="form-control" />
                          </div>
                        </div>
                      </div>                      
                    </div>
                
                
					<div style="height:10px;"></div>


                </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> บันทึก</a>
				</form>
                </div>

            </div>
        </div>
    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>  
        
        
        
      </div>
 
      <?php
      include ("footer.php");

?>
     
    </div>
   
  </div>

</div>

</body>
</html>