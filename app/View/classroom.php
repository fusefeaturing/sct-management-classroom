<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>


</head>
<body>

<?php
include '../Model/config.php';

?>
<div class="container-scroller">

  <?php
include "testheader.php"
?>

  <div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">


      <?php
include "menu.php"
?>

      <div class="content-wrapper">


      <div class="card">
            <div class="card-body">
				<span class="pull-right"><a href="#addnew" data-toggle="modal" class="btn btn-outline-primary" ><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูลห้องเรียน</a></span>
              <h4 class="card-title">ห้องเรียน</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>อาคาร</th>
				                    <th>ห้องเรียน</th>
                            <th>อุปกรณ์</th>
                            <th>ลักษณะโต๊ะ</th>
                            <th>ลักษณะเก้าอี้</th>
                            <th>จำนวนที่นั่ง</th>
                            <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php

$sql = "SELECT cs.classroom_id, cs.classroom_name, bd.building_name, cs.equipment_name, ft.furniture_t_name, fc.furniture_c_name, cs.classroom_num_seat
FROM classroom AS cs
LEFT JOIN building AS bd ON (cs.building_id = bd.building_id)
LEFT JOIN equipment AS eq ON (cs.equipment_id = eq.equipment_id)
LEFT JOIN furniture_t AS ft ON (cs.furniture_t_id = ft.furniture_t_id)
LEFT JOIN furniture_c AS fc ON (cs.furniture_c_id = fc.furniture_c_id)";
$query = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_array($query)) {

    ?>

					<tr>
                        <td><?php echo $row['building_name']; ?></td>
						            <td><?php echo $row['classroom_name']; ?></td>
                        <td><?php echo $row['equipment_name']; ?></td>
                        <td><?php echo $row['furniture_t_name']; ?></td>
                        <td><?php echo $row['furniture_c_name']; ?></td>
                        <td><?php echo $row['classroom_num_seat']; ?></td>


						<td>
							<div align="center"><a href="#read<?php echo $row['classroom_id']; ?>" data-toggle="modal" class="badge badge-info " ><span class="glyphicon glyphicon-edit"></span> ดู</a> &nbsp;
								<a href="#edit<?php echo $row['classroom_id']; ?>" data-toggle="modal" class="badge badge-warning "><span class="glyphicon glyphicon-edit"></span> แก้ไข</a> &nbsp;
								<a href="#del<?php echo $row['classroom_id']; ?>" data-toggle="modal" class="badge badge-danger "><span class="glyphicon glyphicon-trash"></span> ลบ</a> </div>


    <!-- read -->
    <div class="modal fade" id="read<?php echo $row['classroom_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ข้อมูลห้องเรียน</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$data = mysqli_query($conn, "SELECT cs.classroom_id, cs.classroom_name , bd.building_name, cs.equipment_name, ft.furniture_t_name, fc.furniture_c_name, cs.classroom_num_seat
FROM classroom AS cs
LEFT JOIN building AS bd ON (cs.building_id = bd.building_id)
LEFT JOIN equipment AS eq ON (cs.equipment_id = eq.equipment_id)
LEFT JOIN furniture_t AS ft ON (cs.furniture_t_id = ft.furniture_t_id)
LEFT JOIN furniture_c AS fc ON (cs.furniture_c_id = fc.furniture_c_id)
WHERE cs.classroom_id='" . $row['classroom_id'] . "'");
    $drow = mysqli_fetch_array($data);
    /*$upload_dir = '../../public/images/';*/?>
				<div class="form-control">
                    <!--<img src="<?php echo $upload_dir . $drow['image'] ?>" height="200">-->
                    <h5>อาคาร : <strong><?php echo $drow['building_name']; ?></strong></h5>
					          <h5>ห้องเรียน : <strong><?php echo $drow['classroom_name']; ?></strong></h5>
                    <h5>อุปกรณ์ : <strong><?php echo $drow['equipment_name']; ?></strong></h5>
                    <h5>ลักษณะโต๊ะ : <strong><?php echo $drow['furniture_t_name']; ?></strong></h5>
                    <h5>ลักษณะเก้าอี้ : <strong><?php echo $drow['furniture_c_name']; ?></strong></h5>
                    <h5>จำนวนที่นั่ง : <strong><?php echo $drow['classroom_num_seat']; ?></strong></h5>


				</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

	<!-- Delete -->
    <div class="modal fade" id="del<?php echo $row['classroom_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ลบ</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$del = mysqli_query($conn, "select * from classroom where classroom_id='" . $row['classroom_id'] . "'");
    $derow = mysqli_fetch_array($del);?>
				<div class="container-fluid">
                    <h5 align="center">อาคาร : <strong><?php echo $row['building_name']; ?></strong></h5>
					          <h5 align="center">ห้องเรียน : <strong><?php echo $derow['classroom_name']; ?></strong></h5>

                </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <a href="../Controller/process_classroom.php?cmd=delete&id=<?php echo $row['classroom_id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> <!-- form -->
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

<!-- Edit -->
    <div class="modal fade" id="edit<?php echo $row['classroom_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">แก้ไข</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php // mysql select query
    $edit = mysqli_query($conn, "select cr.*  from classroom AS cr
    left join equipment AS eq ON (cr.equipment_id = eq.equipment_id) where classroom_id='" . $row['classroom_id'] . "'");
    $erow = mysqli_fetch_array($edit);
    ?>

                <form class="form-modal" method="POST" action="../Controller/process_classroom.php?cmd=edit&id=<?php echo $row['classroom_id']; ?>"  enctype="multipart/form-data">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">อาคาร</label>
                                  <div class="col-sm-6">
                                  <select class="form-control col-md-7" name="edbd1">
<?php
$sql6 = "select * from building";
    $query6 = mysqli_query($conn, $sql6);
    ?>
                              <?php while ($row0 = mysqli_fetch_array($query6)): ;
                            if (in_array($row['building_name'],$row0)) {
                              echo "<option value='".$row0['building_id']."' selected='selected'> ".$row0['building_name']." </option>";
                            } else {
                              echo "<option value='".$row0['building_id']."'> ".$row0['building_name']." </option>";
                            } endwhile;
                            ?>  

                              

                                </select>
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">ห้องเรียน</label>
                                        <div class="col-sm-8">
                                             <input type="text" name="classroom_name" class="form-control" value="<?php echo $erow['classroom_name']; ?>">
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">อุปกรณ์</label>
                                        <div class="col-sm-10">
                                          <div class="row">
                                        <?php // mysql select query

    $sql5 = "select * from equipment";
    $query5 = mysqli_query($conn, $sql5);
    $edrow = mysqli_fetch_array($query5);

    ?>

<?php //print_r($row);

    $check1 = explode(",", $row['equipment_name']);
//print_r($row['equipment_name']);

    foreach ($query5 as $key => $row5) {
        $check2 = $row5['equipment_name'];
        $check3 = $row5['equipment_id'];

        if (in_array($check2, $check1)) {

            echo "<div class='col-sm-4'><input type='checkbox' name='edcr1[]' id='edcr1[]' value='$check2' checked> $check2</div>";
        } else {
            echo "<div class='col-sm-4'><input type='checkbox' name='edcr1[]' id='edcr1[]' value='$check2' > $check2</div>";
        }

    }

    ?>
        <?php
/*
    $arr = array($row['equipment_name']);

    // $arr=array($row['equipment_name']);
    $dblang = explode(",",$row['equipment_name']);
    $ar = array_slice($dblang,5);
    print_r($ar);
    $checkboxes = isset($dblang) ? $dblang : array();
    foreach($checkboxes as $value) {
    for($i=0;$i<count($check1);$i++){
    if(in_array($value,$check1)){
    echo "<input type='checkbox' name='edcr1[]' id='edcr1[]' value='$value' checked> $value";
    } else {
    echo "<input type='checkbox' name='edcr1[]' id='edcr1[]' value='$value' > $value";
    }

    }

    }

    }
     */
    ?>
                                              </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <?php
    $sql3 = "select * from furniture_t";
    $query2 = mysqli_query($conn, $sql3);
    $sql4 = "select * from furniture_c";
    $query3 = mysqli_query($conn, $sql4);
    ?>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ลักษณะโต๊ะ</label>
                          <div class="col-sm-8">
                          <select class="form-control" name="edcr2">
                            
                          
                            <?php while ($row1 = mysqli_fetch_array($query2)): ;
                            if (in_array($row['furniture_t_name'],$row1)) {
                              echo "<option value='".$row1['furniture_t_id']."' selected='selected'> ".$row1['furniture_t_name']." </option>";
                            } else {
                              echo "<option value='".$row1['furniture_t_id']."'> ".$row1['furniture_t_name']." </option>";
                            } endwhile;
                            ?>  


                                </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ลักษณะเก้าอี้</label>
                          <div class="col-sm-9">
                            <select class="form-control col-md-7" name="edcr3">

                            <?php while ($row3 = mysqli_fetch_array($query3)): ;
                            if (in_array($row['furniture_c_name'],$row3)) {
                              echo "<option value='".$row3['furniture_c_id']."' selected='selected'> ".$row3['furniture_c_name']." </option>";
                            } else {
                              echo "<option value='".$row3['furniture_c_id']."'> ".$row3['furniture_c_name']." </option>";
                            } endwhile;
                            ?>  



                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">จำนวนที่นั่ง</label>
                          <div class="col-sm-5">
                            <input text="text" name="seat_num" class="form-control" value="<?php echo $drow['classroom_num_seat']; ?>">
                          </div>
                        </div>
                      </div>
                    </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-floppy-disk"></span> บันทึก</a>
				</form>

                </div>
            </div>
        </div>
    </div>
    </div>
<!-- /.modal -->

						</td>
					</tr>
					<?php
}

?>
            </tbody>
                    </table>

               <!-- Add New -->
    <div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ระบุห้องเรียนที่ต้องการเพิ่ม</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<div class="container-fluid">
				<!-- form -->
				<form class="form-modal" method="POST" action="../Controller/process_classroom.php?cmd=add"  enctype="multipart/form-data">
					<?php // mysql select query
$sql0 = "select * from building";
$query0 = mysqli_query($conn, $sql0);
?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">อาคาร</label>
                                  <div class="col-sm-6">
                                  <select class="form-control " name="inbd1">
                                        <option>----เลือกอาคาร----</option>
                                            <?php while ($row0 = mysqli_fetch_array($query0)): ;?>

							        							                    <option value="<?php echo $row0['building_id']; ?>"><?php echo $row0['building_name']; ?></option>

							        							                <?php endwhile;?>

                                        </select>
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">ห้องเรียน</label>
                                        <div class="col-sm-8">
                                             <input type="text" name="classroom_name"  class="form-control" />
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">อุปกรณ์</label>
                                        <div class="col-sm-12">
                                        <?php

$sql2 = "select * from equipment";
$query1 = mysqli_query($conn, $sql2);

?>                                <?php while ($row1 = mysqli_fetch_array($query1)): ;?>
								                            <input type="checkbox" name="incr1[]" id="incr1[]" value="<?php echo $row1["equipment_name"]; ?>" requried><?php echo $row1["equipment_name"]; ?>&nbsp;&nbsp;

								                            <?php endwhile;?>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <?php
$sql3 = "select * from furniture_t";
$query2 = mysqli_query($conn, $sql3);
$sql4 = "select * from furniture_c";
$query3 = mysqli_query($conn, $sql4);
?>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ลักษณะโต๊ะ</label>
                          <div class="col-sm-8">
							<select name="incr2" class="form-control" >
									<option>----เลือกลักษณะโต๊ะ----</option>
									<?php while ($row1 = mysqli_fetch_array($query2)): ;?>

									<option value="<?php echo $row1['furniture_t_id']; ?>"><?php echo $row1['furniture_t_name']; ?></option>

										<?php endwhile;?>

							</select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">ลักษณะเก้าอี้</label>
                          <div class="col-sm-7">
							<select name="incr3" class="form-control" >
										<option>---- เลือกลักษณะเก้าอี้ ----</option>
                                        <?php while ($row1 = mysqli_fetch_array($query3)): ;?>

	                                            <option value="<?php echo $row1['furniture_c_id']; ?>"><?php echo $row1['furniture_c_name']; ?></option>

	                                        <?php endwhile;?>
							</select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">จำนวนที่นั่ง</label>
                          <div class="col-sm-5">
                            <input text="text" name="seat_num" class="form-control">
                          </div>
                        </div>
                      </div>
                    </div>



				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-floppy-disk"></span> บันทึก</a>
				</form>
                </div>

            </div>
        </div>
    </div>

</div>


                </div>
              </div>
            </div>
          </div>



      </div>



    </div>

  </div>

</div>
   <?php
include "footer.php";

?>
</div>

</body>
</html>