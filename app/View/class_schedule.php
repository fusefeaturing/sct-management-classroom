<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/jquery-asColorPicker/dist/css/asColorPicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />

</head>
<body>
<?php
//search
include '../Model/config.php';

?>

<div class="container-scroller">

<?php
ini_set('display_errors', 1);
error_reporting(~0);
//error_reporting(~E_NOTICE);

if (isset($_POST['day_start']) && isset($_POST['day_end'])
    && isset($_POST['time_start']) && isset($_POST['time_end']) && isset($_POST['seat_num'])) {

    $day_start = $_POST['day_start'];
    $day_end = $_POST['day_end'];
    $time_start = $_POST['time_start'];
    $time_end = $_POST['time_end'];
    $seat_num = $_POST['seat_num'];

}

?>


    <?php
            include ("testheader.php")
        ?>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
        
        <?php
            include ("menu.php")
        ?>
        
<div class="content-wrapper">  
        <div class="card">
            <div class="card-body">
              <h4 class="card-title">จองห้องเรียน</h4>
              <div class="row">
                <div class="col-12">
                <form name="frmSearch" method="post" action="booking.php">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">วันที่เริ่มต้น</label>
                          <div class="col-sm-9">
                          <input class="form-control " name="day_start" type="date" id="day_start"  value="">
                        </div>
                      </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">วันที่สิ้นสุด</label>
                          <div class="col-sm-9">
                          <input class="form-control " name="day_end" type="date" id="day_end"  value="">
                          </div>
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาเริ่มต้น</label>
                          <div class="input-group clockpicker col-sm-9">
                                <input class="form-control" name="time_start" type="time" id="time_start" value="" />
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">                        
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาสิ้นสุด</label>
                          <div class="input-group clockpicker col-sm-9">
                              <input class="form-control" name="time_end" type="time" id="time_end"  value="">
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>                          
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">จำนวนที่นั่ง</label>
                          <div class="col-sm-9">
                          <input class="form-control" name="seat_num" type="text" id="seat_num"  value="">
                        </div>
                      </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          
                          <div class="col-sm-9">
                          <input type="submit" name="search" value="ค้นหา" class="btn btn-warning">
                        </div>
                      </div>
                      </div>                                           
                    </div>
                </form>

                </div>
              </div>
            </div>
          </div>  
          
          
          
        </div>
        <?php
            include ("footer.php");
        ?>
      </div>
    </div>
  </div>


  <script src="../../public/node_modules/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
  <script src="../../public/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="../../public/js/formpickers.js"></script>
  <script src="../../public/js/formpickers.js"></script>


</body>
</html>