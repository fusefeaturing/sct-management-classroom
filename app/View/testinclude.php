<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Victory Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../public/node_modules/mdi/css/materialdesignicons.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/simple-line-icons/css/simple-line-icons.css" />
  <link rel="stylesheet" href="../../public/node_modules/flag-icon-css/css/flag-icon.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" />
  <link rel="stylesheet" href="../../p">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="../../public/node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css" />
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../../public/css/style.css" />
  <!-- endinject -->
  <link rel="shortcut icon" href="../../public/img/favicon.png" />



  
  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
  <div class="container-scroller">
  
    <?php
            include ("testheader.php")
        ?>

    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
     
        <?php
            include ("menu.php")
        ?>






     
        <div class="content-wrapper">
    
          
          
          
          
        </div>
   
        <?php
        include ("footer.php");

?>
       
      </div>
     
    </div>
 
  </div>


  <script src="../../public/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="../../public/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="../../public/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../public/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="../../public/node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
  <script src="../../public/node_modules/chart.js/dist/Chart.min.js"></script>
  <script src="../../public/node_modules/raphael/raphael.min.js"></script>
  <script src="../../public/node_modules/morris.js/morris.min.js"></script>
  <script src="../../public/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="../../public/js/off-canvas.js"></script>
  <script src="../../public/js/hoverable-collapse.js"></script>
  <script src="../../public/js/misc.js"></script>
  <script src="../../public/js/settings.js"></script>
  <script src="../../public/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="../../public/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
