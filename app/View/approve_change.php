<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../public/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/fontawesome/css/all.min.css">

    <link rel="stylesheet" href="../../public/stylesheets/sheets.css">
     <style>


  </style>

</head>
<body>

<div class="wrapper">

<header class="navbar navbar-expand navbar-dark bg-warning">
        <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>
        <a class="navbar-brand" href="#"><i class="fa fa-code-branch"></i> SCT</a>
    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                    <i class="fa fa-user"></i> <?php echo $_SESSION["member_firstname"] ?>
				</a>
			    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Action</a>
					<a class="dropdown-item" href="../Model/process.php?cmd=logout">ออกจากระบบ</a>

                </div>
            </li>
        </ul>
    </div>
</header>



<div class="d-flex">
  <?php include ("menuadmin.php"); ?>
    <div class="content p-4">

		<div class="container">

        <div class="container-fluid">

            <?php
ini_set('display_errors', 1);
error_reporting(~0);
error_reporting(~E_NOTICE);

?>


<?php
//search
include '../Model/config.php';

?>
<div class="well table-responsive" style="margin:auto; padding:auto; width:auto;">

		<div style="height:20px;"></div>
    <table class="table table-striped table-bordered table-hover">
			<thead style="font-size: 14px;">

                <th>ห้องที่ถูกจอง</th>
                <th>ชื่อผู้จอง</th>
                <th>วันที่เริ่มต้น</th>
                <th>วันที่สิ้นสุด</th>
                <th>เวลาที่เริ่มจอง</th>
                <th>เวลาสิ้นสุด</th>
                <th>จอง ณ วันเวลาที่</th>
                <th>ไม่อนุมัติ</th>


			</thead>
			<tbody>
            <?php
$perpage = 5;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

$start = ($page - 1) * $perpage;


$sql = "SELECT  rq.* ,cr.classroom_name ,m.member_firstname  from request_classroom AS rq
LEFT JOIN classroom AS cr ON (rq.classroom_id = cr.classroom_id)
LEFT JOIN member AS m ON (rq.member_id = m.member_id)
WHERE rq.rq_approve_status = '1' and rq.rq_classroom_id IN 
(SELECT ap.rq_classroom_id  FROM approve AS ap ) limit $start , $perpage";
$query = mysqli_query($conn, $sql) or die("error");
while ($row = mysqli_fetch_array($query)) {
    ?>


            <tr>


            <td><label ><?php echo $row['classroom_name'] ?></label></td>
            <td><label ><?php echo $row['member_firstname'] ?></label></td>
            <td><label ><?php echo $row['rq_day_start'] ?></label></td>
            <td><label ><?php echo $row['rq_day_end'] ?></label></td>
            <td><label ><?php echo $row['rq_time_start'] ?></label></td>
            <td><label ><?php echo $row['rq_time_end'] ?></label></td>
            <td><label ><?php echo $row['rq_date_now'] ?></label></td>
            

            <form action="../Controller/process_approve.php?cmd=noapproom&id" method="post">
            <td> <button type="submit" name="noapprqroom" class="btn btn-danger" value="<?php echo $row['rq_classroom_id']; ?>">ยกเลิกการอนุมัติ </button></td>
            </form>

            </tr>
            <?php
}
?>
            </tbody>
    </table>

<?php
$sql7 = "SELECT  rq.* ,cr.classroom_name ,m.member_firstname  from request_classroom AS rq
LEFT JOIN classroom AS cr ON (rq.classroom_id = cr.classroom_id)
LEFT JOIN member AS m ON (rq.member_id = m.member_id)
WHERE rq.rq_approve_status = '1' and rq.rq_classroom_id  IN
(SELECT ap.rq_classroom_id  FROM approve AS ap ) ";
$query7 = mysqli_query($conn, $sql7) or die("error");
$total_record = mysqli_num_rows($query7);
$total_page = ceil($total_record / $perpage);
?>
<nav>
    <ul class="pagination justify-content-center">
        <li class="page-item ">
            <a class="page-link" href="booking.php?page=1" aria-label="Previous">
                <span aria-hidden="true">Previous</span>
            </a>
        </li>
        <?php for ($i = 1; $i <= $total_page; $i++) {?>
        <li class="page-item "><a class="page-link " href="booking.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php }?>
        <li class="page-item">
            <a class="page-link" href="booking.php?page=<?php echo $total_page; ?>" aria-label="Next">
                <span aria-hidden="true">Next</span>
            </a>
        </li>
    </ul>
</nav>

   </div>


</div>

</div>

    </div>

</div>


</div>









</body>
</html>