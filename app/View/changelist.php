<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../public/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/fontawesome/css/all.min.css">

    <link rel="stylesheet" href="../../public/stylesheets/sheets.css">
     <style>


  </style>

</head>
<body>

<div class="wrapper">

<header class="navbar navbar-expand navbar-dark bg-warning">
        <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>
        <a class="navbar-brand" href="#"><i class="fa fa-code-branch"></i> SCT</a>
    <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                    <i class="fa fa-user"></i> <?php echo $_SESSION["member_firstname"] ?>
				</a>
			    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Action</a>
					<a class="dropdown-item" href="../Model/process.php?cmd=logout">ออกจากระบบ</a>

                </div>
            </li>
        </ul>
    </div>
</header>



<div class="d-flex">
  
        <?php
if ($_SESSION["member_type"] == "admin") {
    include 'menuadmin.php';
} else if ($_SESSION["member_type"] == "professor") {
    include 'menuprofessor.php';
} else if ($_SESSION["member_type"] == "student") {
    include 'menustudent.php';
} else {
    echo "Error Session Menu";
}

?>


       

    <div class="content p-4">

		<div class="container">

        <div class="container-fluid">

            <?php
ini_set('display_errors', 1);
error_reporting(~0);
error_reporting(~E_NOTICE);

if (isset($_POST['day_start']) && isset($_POST['day_end'])
    && isset($_POST['time_start']) && isset($_POST['time_end'])) {

    $day_start = $_POST['day_start'];
    $day_end = $_POST['day_end'];
    $time_start = $_POST['time_start'];
    $time_end = $_POST['time_end'];

}

?>

<!--<form style="float:left;" name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
<div class="form-group">
			<div class="input-group" >

					<input name="day_start" type="date" id="day_start"  value="">
                    <input name="day_end" type="date" id="day_end"  value="">
                    <input name="time_start" type="time" id="time_start"  value="">
                    <input name="time_end" type="time" id="time_end"  value="">
					<input type="submit" name="search" value="search" class="btn btn-warning"></th>
					</div>
			</div>

</form>-->
<?php
//search
include '../Model/config.php';

?>
<div class="well table-responsive" style="margin:auto; padding:auto; ">

		<div style="height:30px;"></div>
    <table class="table table-striped table-bordered table-hover">
			<thead style="font-size: 14px;">


				
                <th>ชื่อผู้เปลี่ยน</th>
                <th>ชื่ออาจารย์</th>
                <th>ห้องเดิม</th>
                <th>วันเดิม</th>                
                <th>เวลาที่เริ่มต้นเดิม</th>
                <th>เวลาสิ้นสุดเดิม</th>
                <th>ห้องเดิม</th>
                <th>วันเดิม</th>                
                <th>เวลาที่เริ่มต้นเดิม</th>
                <th>เวลาสิ้นสุดเดิม</th>
                <th>เปลี่ยนแปลง ณ วันเวลาที่</th>
                



			</thead>
			<tbody>
            <?php
$perpage = 5;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

$start = ($page - 1) * $perpage;



    $sql = "SELECT rq.classroom_id ,cr.classroom_name ,m.member_firstname , rq.rq_day_start ,rq.rq_day_end
    ,rq.rq_time_start , rq.rq_time_end , rq.rq_date_now , rq.rq_approve_status
    FROM request_classroom AS rq
    LEFT JOIN classroom AS cr ON (cr.classroom_id = rq.classroom_id)
    LEFT JOIN member AS m ON (rq.member_id = m.member_id)
    WHERE rq.member_id ORDER BY rq.rq_date_now DESC limit $start , $perpage";


$query = mysqli_query($conn, $sql) or die("error");
while ($row = mysqli_fetch_array($query)) {
    ?>

<?php
if ($row['rq_approve_status'] == "1") {
        $apstatus = "ผ่าน";
    } else if ($row['rq_approve_status'] == "2") {
        $apstatus = "ไม่ผ่าน";
    } else if ($row['rq_approve_status'] == "0") {
        $apstatus = "รอการอนุมัติ";
    } else {

    }
?>


            <tr>





            <td><label ><?php echo $row['classroom_name'] ?></label></td>
            <td><label ><?php echo $row['member_firstname'] ?></label></td>
            <td><label ><?php echo $row['rq_day_start'] ?></label></td>
            <td><label ><?php echo $row['rq_day_end'] ?></label></td>
            <td><label ><?php echo $row['rq_time_start'] ?></label></td>
            <td><label ><?php echo $row['rq_time_end'] ?></label></td>
            <td><label ><?php echo $row['rq_date_now'] ?></label></td>
            <td><label ><?php echo $apstatus ?></label></td>


            </tr>
            <?php
}
?>
            </tbody>
    </table>
    <?php
$sql7 = "SELECT rq.classroom_id ,cr.classroom_name ,m.member_firstname , rq.rq_day_start ,rq.rq_day_end
        ,rq.rq_time_start , rq.rq_time_end , rq.rq_approve_status
        FROM request_classroom AS rq
        LEFT JOIN classroom AS cr ON (cr.classroom_id = rq.classroom_id)
        LEFT JOIN member AS m ON (rq.member_id = m.member_id)
        WHERE rq.member_id ";
$query7 = mysqli_query($conn, $sql7) or die("error");
$total_record = mysqli_num_rows($query7);
$total_page = ceil($total_record / $perpage);
?>
<nav>
    <ul class="pagination justify-content-center">
        <li class="page-item ">
            <a class="page-link" href="bookinglist.php?page=1" aria-label="Previous">
                <span aria-hidden="true">Previous</span>
            </a>
        </li>
        <?php for ($i = 1; $i <= $total_page; $i++) {?>
        <li class="page-item "><a class="page-link " href="bookinglist.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php }?>
        <li class="page-item">
            <a class="page-link" href="bookinglist.php?page=<?php echo $total_page; ?>" aria-label="Next">
                <span aria-hidden="true">Next</span>
            </a>
        </li>
    </ul>
</nav>
   </div>


</div>

</div>

    </div>

</div>


</div>

</body>
</html>