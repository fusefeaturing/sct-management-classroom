<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

	<link rel="stylesheet" href="../../public/node_modules/dropify/dist/css/dropify.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/jquery-file-upload/css/uploadfile.css" />
  
</head>
<body>

<div class="container-scroller">
  
    <?php
            include ("testheader.php")
        ?>

    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
     
        <?php
            include ("menu.php")
        ?>
     
        <div class="content-wrapper">   
          
        <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">ตารางเรียน</h4>
                    <form action="../Controller/process_addclassroom.php?cmd=addscheduleroom" method="post" enctype="multipart/form-data" name="form1">


                          <div class="col-lg-4 grid-margin stretch-card">
                            <div class="card">
                              <div class="card-body">
                                <h4 class="card-title d-flex">เลือกไฟล์ตารางเรียน *.csv</h4>      
                                <input type="file" name="fileCSV" class="dropify" />
                              </div>
                            </div>
                          </div>
                              <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> บันทึก</button>

                    </form>

                             <br> <a href="../Controller/process_addclassroom.php?cmd=delete" class="btn btn-danger"> ลบตารางเรียน</a>

          
        </div>
   
       
       
      </div> 
          
    </div> 
  </div> 
         <?php
        include ("footer.php");
        ?> 
  </div> 
  </div> 


  <script src="../../public/node_modules/dropify/dist/js/dropify.min.js"></script> 
  <script src="../../public/js/dropify.js"></script>

</body>
</html>
