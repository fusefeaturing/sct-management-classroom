<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Victory Admin</title>
  <link rel="stylesheet" href="../../public/node_modules/mdi/css/materialdesignicons.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" />
  
  <link rel="stylesheet" href="../../public/node_modules/simple-line-icons/css/simple-line-icons.css" />
  <link rel="stylesheet" href="../../public/node_modules/flag-icon-css/css/flag-icon.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css" />
  <link rel="stylesheet" href="../../p">
  <link rel="stylesheet" href="../../public/node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css" />
  <link rel="stylesheet" href="../../public/css/style.css" />
  <link rel="shortcut icon" href="../../public/img/favicon.png" />  
 
  <link rel="stylesheet" href="../../public/node_modules/icheck/skins/all.css" />
  <link rel="stylesheet" href="../../public/node_modules/select2/dist/css/select2.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" />  

	<link rel="stylesheet" href="../../public/node_modules/dropify/dist/css/dropify.min.css" />
  <link rel="stylesheet" href="../../public/node_modules/jquery-file-upload/css/uploadfile.css" />
  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
  
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo text-warning" href="class_schedule.php">Classroom Management</a>
        <a class="navbar-brand brand-logo-mini text-warning" href="class_schedule.php">SCT</a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav">
          <li class="nav-item dropdown d-none d-lg-flex">
          
          </li>
        </ul>
       
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>




</body>

</html>
