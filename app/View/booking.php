<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/jquery-asColorPicker/dist/css/asColorPicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
</head>
<body>


<?php

//search
include '../Model/config.php';

?>

<div class="container-scroller">
    <?php
            include ("testheader.php")
        ?>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
        
        <?php
            include ("menu.php")
        ?>
        
<div class="content-wrapper">  
<?php
ini_set('display_errors', 1);
error_reporting(~0);
error_reporting(~E_NOTICE);

if (isset($_POST['day_start']) && isset($_POST['day_end'])
    && isset($_POST['time_start']) && isset($_POST['time_end']) && isset($_POST['seat_num'])) {

    $day_start = $_POST['day_start'];
    $day_end = $_POST['day_end'];
    $time_start = $_POST['time_start'];
    $time_end = $_POST['time_end'];
    $seat_num = $_POST['seat_num'];

   
}

?>
<div style="height:10px;"></div>


  <div class="card">
            <div class="card-body">
              <h4 class="card-title">จองห้องเรียน</h4>
              <div class="row">
                <div class="col-12">
                <form name="frmSearch" method="post" action="booking.php">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">วันที่เริ่มต้น</label>
                          <div class="col-sm-9">
                          <input class="form-control " name="day_start" type="date" id="day_start"  value="<?php echo $day_start ?>">
                        </div>
                      </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">วันที่สิ้นสุด</label>
                          <div class="col-sm-9">
                          <input class="form-control " name="day_end" type="date" id="day_end"  value="<?php echo $day_end ?>">
                          </div>
                        </div>
                      </div>                      
                    </div>
                    <div class="row"> 
                    <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาเริ่มต้น</label>
                          <div class="input-group clockpicker col-sm-9">
                                <input class="form-control" name="time_start" type="time" id="time_start" value="<?php echo $time_start ?>" />
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">                        
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาสิ้นสุด</label>
                          <div class="input-group clockpicker col-sm-9">
                              <input class="form-control" name="time_end" type="time" id="time_end"  value="<?php echo $time_end ?>">
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>                          
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">จำนวนที่นั่ง</label>
                          <div class="col-sm-9">
                          <input class="form-control" name="seat_num" type="text" id="seat_num"  value="<?php echo $seat_num ?>">
                        </div>
                      </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          
                          <div class="col-sm-9">
                          <input type="submit" name="search" value="ค้นหา" class="btn btn-warning">
                        </div>
                      </div>
                      </div>                                           
                    </div>
                </form>

                </div>
              </div>
            </div>
          </div> 

          <div style="height:20px;"></div>

        <div class="card">
            <div class="card-body">
              <h4 class="card-title">ห้องที่สามารถจองได้</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
                            <th>ห้องเรียน</th>
                            <th>สถานะ</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
$day_of_week = date('N', strtotime($day_start));
$day_of_week2 = date('N', strtotime($day_end));
$sql = "SELECT cr.* from classroom as cr where cr.classroom_num_seat>='$seat_num' and cr.classroom_id not in 
( SELECT rq.classroom_id FROM request_classroom AS rq
    WHERE  
      ( rq.rq_day_start BETWEEN '".$day_start."' AND '" . $day_end . "') 
      AND( rq.rq_day_end BETWEEN '".$day_start."' AND '" . $day_end . "')
      AND( rq.rq_time_start  <='" . $time_end . "' )
      and ( rq.rq_time_end  >='" . $time_start . "')
     and (rq.rq_approve_status = '0' or rq.rq_approve_status = '1') ) and cr.classroom_name not in    
    ( SELECT  sc.schedule_name FROM SCHEDULEROOM AS sc
    WHERE 
      (sc.schedule_day_start BETWEEN '".$day_of_week."' AND  '".$day_of_week2."')
     AND(sc.schedule_day_end BETWEEN '".$day_of_week."' AND  '".$day_of_week2."')
     AND(sc.schedule_time_start <='" . $time_end . "' ) 
     AND( sc.schedule_time_end  >='" . $time_start . "' ) 
)  GROUP BY  cr.classroom_name  ";
$query = mysqli_query($conn, $sql) or die("error" );


while ($row = mysqli_fetch_array($query)) {
    ?>
        <tr>
           <td>

            <a href="#booking<?php echo $row['classroom_id']; ?>" data-toggle="modal" class="btn btn-outline-success" >
            <span class="glyphicon glyphicon-edit"></span> <?php echo $row['classroom_name'] ?></a> &nbsp;

            <form action="../Controller/process_class_schedule.php?cmd=addroom" method="post">

            <input type="hidden" name="day_start" value="<?php echo $day_start ?>">
            <input type="hidden" name="day_end" value="<?php echo $day_end ?>">
            <input type="hidden" name="time_start" value="<?php echo $time_start ?>">
            <input type="hidden" name="time_end" value="<?php echo $time_end ?>">   
            <td>
              <label class="badge badge-info">ว่าง</label>
            </td>

            <td>
                <a href="#booking<?php echo $row['classroom_id']; ?>" data-toggle="modal" class="btn btn-outline-success" >
            <span class="glyphicon glyphicon-edit"></span> จอง</a>                              
            </td>
            </form>
           </td> 
        </tr> 
<!-- read -->
    <div class="modal fade" id="booking<?php echo $row['classroom_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">

           
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ยืนยันรายการจองห้อง</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$sql2 = "SELECT cs.classroom_id,cs.classroom_name, cs.classroom_num_seat, eq.equipment_name, ft.furniture_t_name, fc.furniture_c_name, cs.classroom_num_seat
                FROM classroom AS cs
                LEFT JOIN equipment AS eq ON (cs.equipment_id = eq.equipment_id)
                LEFT JOIN furniture_t AS ft ON (cs.furniture_t_id = ft.furniture_t_id)
                LEFT JOIN furniture_c AS fc ON (cs.furniture_c_id = fc.furniture_c_id)
                where cs.classroom_id='" . $row['classroom_id'] . "' ";
    $data = mysqli_query($conn, $sql2);
    $drow = mysqli_fetch_array($data);
    ?>
				<div class="form-control">

					<h5>ชื่อผู้จอง : <strong><?php echo $_SESSION["member_firstname"] ?></strong></h5>
					<h5>ห้องเรียน : <strong><?php echo $drow['classroom_name'] ?></strong></h5>
                    <h5>วันที่เริ่มต้น : <strong><?php echo $day_start ?></strong></h5>
                    <h5>วันที่สิ้นสุด : <strong><?php echo $day_end ?></strong></h5>
                    <h5>เวลาเริ่มต้น : <strong><?php echo $time_start ?></strong></h5>
                    <h5>เวลาสิ้นสุด : <strong><?php echo $time_end ?></strong></h5>
                    <h5>จำนวนที่นั่ง : <strong><?php echo $drow['classroom_num_seat'] ?></strong></h5>


                    <form action="../Controller/process_class_schedule.php?cmd=addroom" method="post">
                        
                        <input type="hidden" name="day_start" value="<?php echo $day_start ?>">
                        <input type="hidden" name="day_end" value="<?php echo $day_end ?>">
                        <input type="hidden" name="time_start" value="<?php echo $time_start ?>">
                        <input type="hidden" name="time_end" value="<?php echo $time_end ?>">




				</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" name="request" value="<?php echo $drow['classroom_id'] ?>" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> ตกลง</button>
                </div>
                    </form>
            </div>
        </div>
    </div>



            <?php
}
?>
            </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
          </div>  
          
          
          
        </div>
        <?php
            include ("footer.php");
        ?>
      </div>
    </div>
  </div> 


  <script src="../../public/node_modules/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
  <script src="../../public/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="../../public/js/formpickers.js"></script>
  <script src="../../public/js/formpickers.js"></script>
</body>
</html>