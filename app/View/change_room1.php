<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/jquery-asColorPicker/dist/css/asColorPicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />

</head>
<body>
<?php
//search
include '../Model/config.php';

?>


            <?php
ini_set('display_errors', 1);
error_reporting(~0);
error_reporting(~E_NOTICE);

if ($day7 == "1") {
    $newday = "วันจันทร์";
} else if ($day7 == "2") {
    $newday = "วันอังคาร";
} else if ($day7 == "3") {
    $newday = "วันพุธ";
} else if ($day7 == "4") {
    $newday = "วันพุฤหัสบดี";
} else if ($day7 == "5") {
    $newday = "วันศุกร์";
} else if ($day7 == "6") {
    $newday = "วันเสาร์";
} else if ($day7 == "7") {
    $newday = "วันอาทิตย์";
} else {

}

?>
<div class="container-scroller">
  
    <?php
            include ("testheader.php")
        ?>

    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
     
        <?php
            include ("menu.php")
        ?>
     
        <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
              <h4 class="card-title">เปลี่ยนแปลงห้องเรียน</h4>
              <div class="row">
                <div class="col-12 ">
                    <form name="frmSearch" method="post" action="change_room2.php">
                    <div class="row justify-content-center">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เลือกวัน</label>
                          <div class="col-sm-9">
                          <select class="form-control " name="day7" required>
                                                 <option value=""></option>
                                                 <option value="1">วันจันทร์</option>
                                                 <option value="2">วันอังคาร</option>
                                                 <option value="3">วันพุธ</option>
                                                 <option value="4">วันพฤหัสบดี</option>
                                                 <option value="5">วันศุกร์</option>
                                                 <option value="6">วันเสาร์</option>
                                                 <option value="7">วันอาทิตย์</option>
                                     </select> 
                          </div>
                       </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาเริ่มต้น</label>
                          <div class="input-group clockpicker col-sm-9">
                                <input class="form-control" name="time_start" type="time" id="time_start" value="" />
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">                        
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาสิ้นสุด</label>
                          <div class="input-group clockpicker col-sm-9">
                              <input class="form-control" name="time_end" type="time" id="time_end"  value="">
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>                          
                        </div>
                      </div>  
                   
                    </div>
                     <div class="row justify-content-center">                      
                      <div class="col-md-1 ">
                        <div class="form-group row">                          
                          <div class="col-sm-9">
                          <input type="submit" name="search" value="ค้นหา" class="btn btn-warning">
                          </div>
                        </div>
                      </div>                      
                    </div>
                  </form>   
                </div>                            
                  
                    
              </div>
              </div>
            </div>
          </div>     
       
      </div>     
    </div> 
  </div>
</div>

<?php
            include ("footer.php");
        ?>


  <script src="../../public/node_modules/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
  <script src="../../public/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="../../public/js/formpickers.js"></script>
  <script src="../../public/js/formpickers.js"></script>
</body>
</html>
