<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--<link rel="stylesheet" href="../../public/stylesheets/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/fontawesome/css/all.min.css">

    <link rel="stylesheet" href="../../public/stylesheets/sheets.css">-->
     <style>


  </style>
   

</head>
<body>

<?php
//search
include '../Model/config.php';

?>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <?php
            include ("testheader.php")
        ?>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
        <!-- partial:partials/_sidebar.html -->
      
<?php

    include 'menu.php';


?>
        <!-- partial -->
<div class="content-wrapper">
    

        <div class="card">
            <div class="card-body">
              <h4 class="card-title">รายการจองห้อง</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
                            <th>ห้องที่ถูกจอง</th>
                            <th>ชื่อผู้จอง</th>
                            <th>วันที่เริ่มต้น</th>
                            <th>วันที่สิ้นสุด</th>
                            <th>เวลาที่เริ่มจอง</th>
                            <th>เวลาสิ้นสุด</th>
                            <th>จอง ณ วันเวลาที่</th>
                            <th>สถานะ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php

if ($_SESSION["member_type"] == "admin") {

$sql = "SELECT rq.classroom_id ,cr.classroom_name ,m.member_firstname , rq.rq_day_start ,rq.rq_day_end
,rq.rq_time_start , rq.rq_time_end , rq.rq_date_now , rq.rq_approve_status
FROM request_classroom AS rq
LEFT JOIN classroom AS cr ON (cr.classroom_id = rq.classroom_id)
LEFT JOIN member AS m ON (rq.member_id = m.member_id)
WHERE rq.member_id ORDER BY rq.rq_date_now DESC ";

} else if ($_SESSION["member_type"] == "professor") {

$sql = "SELECT rq.classroom_id ,cr.classroom_name ,m.member_firstname , rq.rq_day_start ,rq.rq_day_end
,rq.rq_time_start , rq.rq_time_end , rq.rq_date_now , rq.rq_approve_status
FROM request_classroom AS rq
LEFT JOIN classroom AS cr ON (cr.classroom_id = rq.classroom_id)
LEFT JOIN member AS m ON (rq.member_id = m.member_id)
WHERE rq.member_id = '" . $_SESSION['member_id'] . "' ORDER BY rq.rq_date_now  DESC ";

} else if ($_SESSION["member_type"] == "student") {

$sql = "SELECT rq.classroom_id ,cr.classroom_name ,m.member_firstname , rq.rq_day_start ,rq.rq_day_end
,rq.rq_time_start , rq.rq_time_end , rq.rq_date_now , rq.rq_approve_status
FROM request_classroom AS rq
LEFT JOIN classroom AS cr ON (cr.classroom_id = rq.classroom_id)
LEFT JOIN member AS m ON (rq.member_id = m.member_id)
WHERE rq.member_id = '" . $_SESSION['member_id'] . "' ORDER BY rq.rq_date_now  DESC ";

} else {
echo "Error Session approve-list";
}

$query = mysqli_query($conn, $sql) or die("error");
while ($row = mysqli_fetch_array($query)) {
?>

        <?php
        if ($row['rq_approve_status'] == "1") {
        $apstatus = "อนุมัติ";
        } else if ($row['rq_approve_status'] == "2") {
        $apstatus = "ไม่อนุมัติ";
        } else if ($row['rq_approve_status'] == "0") {
        $apstatus = "รอการอนุมัติ";
        } else if ($row['rq_approve_status'] == "3") {
        $apstatus = "ยกเลิกการจองแล้ว"; 
        }else {
        
        }
        ?>


    <tr>





    <td><label ><?php echo $row['classroom_name'] ?></label></td>
    <td><label ><?php echo $row['member_firstname'] ?></label></td>
    <td><label ><?php echo $row['rq_day_start'] ?></label></td>
    <td><label ><?php echo $row['rq_day_end'] ?></label></td>
    <td><label ><?php echo $row['rq_time_start'] ?></label></td>
    <td><label ><?php echo $row['rq_time_end'] ?></label></td>
    <td><label ><?php echo $row['rq_date_now'] ?></label></td>
    <td><label ><?php echo $apstatus ?></label></td>


    </tr>
    <?php
}
?>
            </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
          </div>          
</div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2017 <a href="#">UrbanUI</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- row-offcanvas ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

</body>
</html>