<?php
session_start();
if ($_SESSION['member_id'] == "") {
    echo "Please Login!";
    exit();
}

include '../Model/config.php';


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>

<div class="container-scroller">
    <?php
            include ("testheader.php")
        ?>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
        
        <?php
            include ("menu.php")
        ?>
        
<div class="content-wrapper">    
        
<?php
        $sql = "SELECT m.member_id, m.member_username, m.member_firstname,m.member_lastname ,m.member_tel,m.member_type, 
                        m.member_profile_pic,b.branch_id, b.branch_name , f.faculty_id, f.faculty_name 
                FROM member AS m
                left join branch AS b ON (m.branch_id = b.branch_id)
                left join faculty AS f ON (m.faculty_id = f.faculty_id)
                WHERE m.member_id = '" . $_SESSION['member_id'] . "' ";

        $query = mysqli_query($conn, $sql);
        while ($row = mysqli_fetch_array($query)) {
    ?>



 <?php

	$edit = mysqli_query($conn, "select * from member where member_id='" . $row['member_id'] . "'");
    $erow = mysqli_fetch_array($edit);
	$upload_dir = '../../public/images/';
	?>


			<div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">แก้ไขข้อมูลผู้ใช้</h4>
                  <form class="form-sample" method="POST" action="../Controller/process_member.php?cmd=edit&id=<?php echo $erow['member_id']; ?>" enctype="multipart/form-data">
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">รหัสผู้ใช้งาน</label>
                          <div class="col-sm-9">
                            <label name="username" class="form-control"><?php echo $row['member_username']; ?></label>
                            
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">รหัสผ่าน</label>
                          <div class="col-sm-9">
                            <input type="password" name="password" value="<?php echo $erow['member_password']; ?>" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
						            <div class="col-md-6">
                    	    <div class="form-group row">
                    	      <label class="col-sm-3 col-form-label">ชื่อ</label>
                    	      <div class="col-sm-9">
                    	        <input type="text" name="firstname"  value="<?php echo $erow['member_firstname']; ?>" class="form-control" />
                    	      </div>
                    	    </div>
                    	  </div>                    	
					            	<div class="col-md-6">
                    	    <div class="form-group row">
                    	      <label class="col-sm-3 col-form-label">นามสกุล</label>
                    	      <div class="col-sm-9">
                    	        <input type="text" name="lastname" value="<?php echo $erow['member_lastname']; ?>" class="form-control" />
                    	      </div>
                    	    </div>
                    	  </div> 
					</div>
					<?php
	$sql1 = "select * from faculty";
    $query1 = mysqli_query($conn, $sql1);
    $sql2 = "select * from branch";
    $query2 = mysqli_query($conn, $sql2);
    ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">คณะ</label>
                          <div class="col-sm-9">
							<select name="faculty" class="form-control" >
              <?php while ($row1 = mysqli_fetch_array($query1)): ;
                            if (in_array($row['faculty_name'],$row1)) {
                              echo "<option value='".$row1['faculty_id']."' selected='selected'> ".$row1['faculty_name']." </option>";
                            } else {
                              echo "<option value='".$row1['faculty_id']."'> ".$row1['faculty_name']." </option>";
                            } endwhile;
                            ?> 
								
							</select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">สาขา</label>
                          <div class="col-sm-9">
							<select name="branch" class="form-control" >
              <?php while ($row2 = mysqli_fetch_array($query2)): ;
                            if (in_array($row['branch_name'],$row2)) {
                              echo "<option value='".$row2['branch_id']."' selected='selected'> ".$row2['branch_name']." </option>";
                            } else {
                              echo "<option value='".$row2['branch_id']."'> ".$row2['branch_name']." </option>";
                            } endwhile;
                            ?>  
							
							</select>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เบอร์โทรศัพท์</label>
                          <div class="col-sm-9">
                            <input type="text" name="tel"  value="<?php echo $erow['member_tel']; ?>" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ประเภทสมาชิก</label>
                          <div class="col-sm-9">
						                <label name="member_type" class="form-control"><?php echo $erow['member_type']; ?></label>                            
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
				            	<div class="col-md-6">
                        <div class="form-group row">
                        	<label class="col-sm-3 col-form-label">รูปโปรไฟล์</label>
                        	  	<div class="col-sm-9">
										<img src="<?php echo $upload_dir . $erow['member_profile_pic'] ?>" height="100">	
										<div style="height:10px;"></div>
                    			 		<input type="file" name="image" class="file-upload-default" value="<?php echo $upload_dir . $erow['member_profile_pic'] ?> "/>
                    			  	<div class="input-group col-xs-12">
                    			    	<input type="text" class="form-control file-upload-info" disabled="" placeholder="" />
                    			    		<span class="input-group-btn">
                    			    		  <button class="file-upload-browse btn btn-warning" type="button">เลือกรูปโปรไฟล์</button>
                    		   				</span>                   
                        	  		</div>
                        		</div>
                      	</div>					  
					            </div>					
                    </div>
                    
						<div align="right">
                   			<button type="button" class="btn btn-default" ><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                   			<button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> บันทึก</button>
               			</div>
                    </div>
                    
                  </form>
                </div>
              </div>
            </div>			
			
				
        <?php 
		} 
		?>          
          
          
        </div>
        <?php
            include ("footer.php");
        ?>
      </div>
    </div>
  






</body>
</html>





