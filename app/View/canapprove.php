<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  
     <style>


  </style>

</head>
<body>

<?php
//search
include '../Model/config.php';

?>
<div class="container-scroller">
  
    <?php
            include ("testheader.php")
        ?>

    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
     
        <?php
            include ("menu.php")
        ?>
     
        <div class="content-wrapper">
    
        <div class="card">
            <div class="card-body">
              <h4 class="card-title">ยกเลิกการอนุมัติ</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
                            <th>ห้องที่ถูกจอง</th>
                            <th>ชื่อผู้จอง</th>
                            <th>วันที่เริ่มต้น</th>
                            <th>วันที่สิ้นสุด</th>
                            <th>เวลาที่เริ่มจอง</th>
                            <th>เวลาสิ้นสุด</th>
                            <th>จอง ณ วันเวลาที่</th>
                            <th>ยกเลิกการอนุมัติ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php

$sql = "SELECT  rq.* ,cr.classroom_name ,m.member_firstname  from request_classroom AS rq
LEFT JOIN classroom AS cr ON (rq.classroom_id = cr.classroom_id)
LEFT JOIN member AS m ON (rq.member_id = m.member_id)
WHERE rq.rq_approve_status = '1' and rq.rq_classroom_id IN 
(SELECT ap.rq_classroom_id  FROM approve AS ap ) ";
$query = mysqli_query($conn, $sql) or die("error");
while ($row = mysqli_fetch_array($query)) {
    ?>


            <tr>
 
            <td><label ><?php echo $row['classroom_name'] ?></label></td>
            <td><label ><?php echo $row['member_firstname'] ?></label></td>
            <td><label ><?php echo $row['rq_day_start'] ?></label></td>
            <td><label ><?php echo $row['rq_day_end'] ?></label></td>
            <td><label ><?php echo $row['rq_time_start'] ?></label></td>
            <td><label ><?php echo $row['rq_time_end'] ?></label></td>
            <td><label ><?php echo $row['rq_date_now'] ?></label></td>
            

            <form action="../Controller/process_approve.php?cmd=canapproom&id" method="post">
            <td> <button type="submit" name="canapprqroom" class="btn btn-danger" value="<?php echo $row['rq_classroom_id']; ?>">ยกเลิกการอนุมัติ </button></td>
            </form>

            </tr>
            <?php
}
?>
            </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
          </div>       
          
        </div>
   
        <?php
        include ("footer.php");

?>
       
      </div>     
    </div> 
  </div>

</body>
</html>