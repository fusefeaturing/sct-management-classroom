<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <title>Hello, world!</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


</head>
<body>


<?php
//search
include '../Model/config.php';

?>
<div class="container-scroller">
  
    <?php
            include ("testheader.php")
        ?>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
        
        <?php
            include ("menu.php")
        ?>
        
<div class="content-wrapper">  
<?php
ini_set('display_errors', 1);
error_reporting(~0);
?>



<?php

$sql = "SELECT m.member_id, m.member_username, m.member_firstname,m.member_lastname ,m.member_tel,m.member_type,
m.member_profile_pic,b.branch_id, b.branch_name ,f.faculty_id, f.faculty_name FROM member AS m
left join branch AS b ON (m.branch_id = b.branch_id)
left join faculty AS f ON (m.faculty_id = f.faculty_id)
 ORDER BY m.member_id DESC ";

$query = mysqli_query($conn, $sql);

?>
  
        <div class="card">
            <div class="card-body">
            <span class="pull-right"><a href="#addnew" data-toggle="modal" class="btn btn-outline-primary" ><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูลผู้ใช้งาน</a></span>
              <h4 class="card-title">ข้อมูลผู้ใช้งาน</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
							<th>ชื่อ</th>
							<th>สาขา</th>
							<th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php

while ($row = mysqli_fetch_array($query)) {
    ?>
					<tr>
						<td><?php echo $row['member_firstname']; ?></td>
						<td><?php echo $row['branch_name']; ?></td>
						<td>
						<div class="btrp" align="center"><a href="#read<?php echo $row['member_id']; ?>" data-toggle="modal" class="btn btn-info  " ><span class="glyphicon glyphicon-edit"></span> ดู</a> &nbsp;
								<a href="#edit<?php echo $row['member_id']; ?>" data-toggle="modal" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> แก้ไข</a> &nbsp;
								<a href="#del<?php echo $row['member_id']; ?>" data-toggle="modal" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> </div>

    <!-- read -->
    <div class="modal fade" id="read<?php echo $row['member_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ข้อมูลผู้ใช้งาน</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$data = mysqli_query($conn, "select * from member where member_id='" . $row['member_id'] . "'");
    $drow = mysqli_fetch_array($data);
    $upload_dir = '../../public/images/';?>
					<div class="container-fluid">

             				<img src="<?php echo $upload_dir . $drow['member_profile_pic'] ?>" height="100">
							<h5>ชื่อ: <strong><?php echo $drow['member_firstname']; ?></strong></h5>
							<h5>นามสกุล: <strong><?php echo $drow['member_lastname']; ?></strong></h5>
							<h5>คณะ: <strong><?php echo $row['faculty_name']; ?></strong></h5>
							<h5>สาขา: <strong><?php echo $row['branch_name']; ?></strong></h5>
							<h5>เบอร์โทรศัพท์: <strong><?php echo $drow['member_tel']; ?></strong></h5>
							<h5>ประเภทสมาชิก: <strong><?php echo $drow['member_type']; ?></strong></h5>

					</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

	<!-- Delete -->
    <div class="modal fade" id="del<?php echo $row['member_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ลบ</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$del = mysqli_query($conn, "select * from member where member_id='" . $row['member_id'] . "'");
    $drow = mysqli_fetch_array($del);?>
				<div class="container-fluid">
					<h5 align="center">ชื่อ: <strong><?php echo $drow['member_firstname']; ?></strong></h5>
                </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <a href="../Controller/process_member.php?cmd=delete&id=<?php echo $row['member_id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> <!-- form -->
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

<!-- Edit -->
    <div class="modal fade" id="edit<?php echo $row['member_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">แก้ไข</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$edit = mysqli_query($conn, "select * from member where member_id='" . $row['member_id'] . "'");
	$erow = mysqli_fetch_array($edit);
	?>
				
				<form class="form-modal" method="POST" action="../Controller/process_member.php?cmd=edit&id=<?php echo $erow['member_id']; ?>" enctype="multipart/form-data"> <!-- form -->
				            <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">รหัสผู้ใช้งาน</label>
                          <div class="col-sm-8">
                            <input name="username" value="<?php echo $erow['member_username']; ?>" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">รหัสผ่าน</label>
                          <div class="col-sm-9">
                            <input type="password" name="password" value="<?php echo $erow['member_password']; ?>" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
				

					
					          <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ชื่อ</label>
                          <div class="col-sm-8">
                            <input name="firstname" value="<?php echo $erow['member_firstname']; ?>" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">นามสกุล</label>
                          <div class="col-sm-9">
                            <input type="text" name="lastname" value="<?php echo $erow['member_lastname']; ?>" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>			
				
					
					<?php
$sql1 = "select * from faculty";
$query1 = mysqli_query($conn, $sql1);
$sql2 = "select * from branch";
$query2 = mysqli_query($conn, $sql2);
?>
					          <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">คณะ</label>
                          <div class="col-sm-8">
							              <select name="faculty" class="form-control" >
                            <?php while ($row1 = mysqli_fetch_array($query1)): ;
                                          if (in_array($row['faculty_name'],$row1)) {
                                            echo "<option value='".$row1['faculty_id']."' selected='selected'> ".$row1['faculty_name']." </option>";
                                          } else {
                                            echo "<option value='".$row1['faculty_id']."'> ".$row1['faculty_name']." </option>";
                                          } endwhile;
                                          ?>  

							              </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">สาขา</label>
                          <div class="col-sm-9">
							              <select name="branch" class="form-control" >                                                      
                            <?php while ($row2 = mysqli_fetch_array($query2)): ;
                                          if (in_array($row['branch_name'],$row2)) {
                                            echo "<option value='".$row2['branch_id']."' selected='selected'> ".$row2['branch_name']." </option>";
                                          } else {
                                            echo "<option value='".$row2['branch_id']."'> ".$row2['branch_name']." </option>";
                                          } endwhile;
                                          ?>  

							              </select>
                          </div>
                        </div>
                      </div>
					          </div>
										
						

					          <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เบอร์โทรศัพท์</label>
                          <div class="col-sm-8">
                            <input type="text" name="tel" value="<?php echo $erow['member_tel']; ?>" class="form-control" />
                          </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">ประเภทสมาชิก</label>
                              <div class="col-sm-9">
						                      <select class="form-control" name="member_type" required>
								                    <option selected='selected' ><?php echo $row['member_type'] ?></option>
          						              <option value="admin">admin</option>
		  						                  <option value="student">student</option>
		  						                  <option value="professor">professor</option>
        				                  </select>
                              </div>
                          </div>
                        </div>
                    
                  </div>
                      
                          
                          
                    		
				
					
					            <div class="row">
				    	          <div class="col-md-6">
                        	<div class="form-group row">
                        		<label class="col-sm-3 col-form-label">รูปโปรไฟล์</label>
                        		  	<div class="col-sm-9">
											            <img src="<?php echo $upload_dir . $erow['member_profile_pic'] ?>" height="100">	
											            <div style="height:10px;"></div>
                    				 		<input type="file" name="image" class="file-upload-default" value="<?php echo $upload_dir . $erow['member_profile_pic'] ?> "/>
                    				  	<div class="input-group col-xs-12">
                    				    	<input type="text" class="form-control file-upload-info" disabled="" placeholder="" />
                    				    		<span class="input-group-btn">
                    				    		  <button class="file-upload-browse btn btn-warning" type="button">เลือกรูปโปรไฟล์</button>
                    			   				</span>                   
                        		  		</div>
                        			</div>
                      		</div>					  
						            </div>											
					            </div>
                
				
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> บันทึก</button>
                </div>
        </form>
      </div>
      </div>
    </div>
  </div>
    
          
<!-- /.modal -->

						</td>
					</tr>
					<?php
}

?>
<!-- Add New -->
<div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">เพิ่มข้อมูลผู้ใช้งาน</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<div class="container-fluid">
				<!-- form -->
				<form class="form-modal" method="POST" action="../Controller/process_member.php?cmd=add"  enctype="multipart/form-data">
				<div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">รหัสผู้ใช้งาน</label>
                          <div class="col-sm-8">
                            <input name="username"  class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">รหัสผ่าน</label>
                          <div class="col-sm-9">
                            <input type="password" name="password"  class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>


					<div style="height:10px;"></div>
					<div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ชื่อ</label>
                          <div class="col-sm-8">
						  	<input type="text" name="firstname" class="form-control"> 	
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">นามสกุล</label>
                          <div class="col-sm-9">
						  	<input type="text" name="lastname" class="form-control">
                          </div>
                        </div>
                      </div>
                    </div>

					<div style="height:20px;"></div>
				
					
					<?php
$sql1 = "select * from faculty";
$query1 = mysqli_query($conn, $sql1);
$sql2 = "select * from branch";
$query2 = mysqli_query($conn, $sql2);
?>

					          <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">คณะ</label>
                          <div class="col-sm-8">
							<select name="faculty" class="form-control" >
									<option>-------------- เลือกคณะ --------------</option>
									<?php while ($row1 = mysqli_fetch_array($query1)): ;?>																	

										<option value="<?php echo $row1['faculty_id'] ?>"><?php echo $row1['faculty_name'] ?></option>
																			<?php endwhile;?>
							</select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">สาขา</label>
                          <div class="col-sm-9">
							<select name="branch" class="form-control" >
										<option>--------------- เลือกสาขา --------------</option>
									<?php while ($row2 = mysqli_fetch_array($query2)): ;?>																	

										<option value="<?php echo $row2['branch_id'] ?>"><?php echo $row2['branch_name'] ?></option>
																			<?php endwhile;?>
							</select>
                          </div>
                        </div>
                      </div>
                    </div>

					<div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เบอร์โทรศัพท์</label>
                          <div class="col-sm-8">
                            <input type="text" name="tel"  class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ประเภทสมาชิก</label>
                          <div class="col-sm-9">
						  <select class="form-control" name="member_type" required>
								<option>ประเภทสมาชิก</option>
          						<option value="admin">admin</option>
		  						<option value="student">student</option>
		  						<option value="professor">professor</option>
        				</select>
                          </div>
                        </div>
                      </div>
                    </div>

					<div style="height:10px;"></div>					
					
					            <div class="row">
				    	          <div class="col-md-6">
                        	<div class="form-group row">
                        		<label class="col-sm-3 col-form-label">รูปโปรไฟล์</label>
                        		  	<div class="col-sm-9">			
                    				 		<input type="file" name="image" class="file-upload-default" />
                    				  	<div class="input-group col-xs-12">
                    				    	<input type="text" class="form-control file-upload-info" disabled="" placeholder="" />
                    				    		<span class="input-group-btn">
                    				    		  <button class="file-upload-browse btn btn-warning" type="button">เลือกรูปโปรไฟล์</button>
                    			   				</span>                   
                        		  		</div>
                        			</div>
                      		</div>					  
						            </div>											
					            </div>
					
                </div>
				</div>
                <div class="modal-footer inputbox">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> บันทึก</a>

                </div>
				</form>
            </div>
        </div>
    </div>
            </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
          </div>  
          
          
          
        </div>
        <?php
            include ("footer.php");
        ?>
      </div>
    </div>
  </div>




</body>
</html>
