<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

</head>
<body>


<div class="container-scroller">
  
    <?php
            include ("testheader.php")
        ?>

    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
     
        <?php
            include ("menu.php")
        ?>

     
        <div class="content-wrapper">
            <div class="card">
            <div class="card-body">				
				<span class="pull-right"><a href="#addnew" data-toggle="modal" class="btn btn-outline-primary" ><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูลลักษณะโต๊ะ</a></span>
              <h4 class="card-title">ลักษณะเก้าอี้</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
                            <th>ลักษณะเก้าอี้</th>
				            <th></th>
                        </tr>
                      </thead>                      
                      <tbody>
                      <?php
include '../Model/config.php';

$sql = "SELECT * FROM furniture_c ORDER BY furniture_c_id DESC";

$query = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_array($query)) {
    ?>
					<tr>
						<td><?php echo $row['furniture_c_name']; ?></td>

						<td>
						<div align="center"><a href="#read<?php echo $row['furniture_c_id']; ?>" data-toggle="modal" class="btn btn-info " ><span class="glyphicon glyphicon-edit"></span> ดู</a> &nbsp;
								<a href="#edit<?php echo $row['furniture_c_id']; ?>" data-toggle="modal" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> แก้ไข</a> &nbsp;
								<a href="#del<?php echo $row['furniture_c_id']; ?>" data-toggle="modal" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> </div>

    <!-- read -->
    <div class="modal fade" id="read<?php echo $row['furniture_c_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">เก้าอี้</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$data = mysqli_query($conn, "select * from furniture_c where furniture_c_id='" . $row['furniture_c_id'] . "'");
    $drow = mysqli_fetch_array($data);
    /*$upload_dir = '../../public/images/';*/?>
				<div class="container-fluid">
                    <!--<img src="<?php echo $upload_dir . $drow['image'] ?>" height="200">-->
					
					<h5>ลักษณะเก้าอี้ : <strong><?php echo $drow['furniture_c_name']; ?></strong></h5>

				</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

	<!-- Delete -->
    <div class="modal fade" id="del<?php echo $row['furniture_c_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ลบ</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$del = mysqli_query($conn, "select * from furniture_c where furniture_c_id='" . $row['furniture_c_id'] . "'");
    $drow = mysqli_fetch_array($del);?>
				<div class="container-fluid">
					<h5 align="center">ลักษณะเก้าอี้ : <strong><?php echo $drow['furniture_c_name']; ?></strong></h5>
                </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <a href="../Controller/process_chair.php?cmd=delete&id=<?php echo $row['furniture_c_id']; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a> <!-- form -->
                </div>

            </div>
        </div>
    </div>
<!-- /.modal -->

<!-- Edit -->
    <div class="modal fade" id="edit<?php echo $row['furniture_c_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">แก้ไข</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$edit = mysqli_query($conn, "select * from furniture_c where furniture_c_id='" . $row['furniture_c_id'] . "'");
    $erow = mysqli_fetch_array($edit);?>
			
					<form class="form-modal" method="POST" action="../Controller/process_chair.php?cmd=edit&id=<?php echo $erow['furniture_c_id']; ?>" enctype="multipart/form-data"> <!-- form -->
                    
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">ลักษณะเก้าอี้</label>
                          <div class="col-sm-9">
                            <input text="text" name="furniture_c_name" class="form-control" value="<?php echo $erow['furniture_c_name']; ?>"/>
                          </div>
                        </div>
                      </div>                      
                    </div> 

               	 
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> บันทึก</button>
                </div>
				</form>
            </div>
        </div>
    </div>
<!-- /.modal -->

						</td>
					</tr>
					<?php
}

?>
            </tbody>
                    </table>
	<!-- Add New -->
    <div class="modal fade" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ระบุลักษณะเก้าอี้ที่ต้องการเพิ่ม</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<div class="container-fluid">
				<!-- form -->
				<form class="form-modal" method="POST" action="../Controller/process_chair.php?cmd=add"  enctype="multipart/form-data">
                    
                    <div class="row">
                      <div class="col-md-10">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">ลักษณะเก้าอี้</label>
                          <div class="col-sm-8">
                            <input text="text" name="furniture_c_name" class="form-control" >
                          </div>
                        </div>
                      </div>                      
                    </div> 
                </div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-floppy-disk"></span> บันทึก</a>
				</form>
                </div>

            </div>
        </div>
    </div>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>  
          
          
          
          
        </div>
   
        <?php
        include ("footer.php");

?>
       
      </div>
     
    </div>
 
</div> 

</body>
</html>