<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>





</head>
<body>
<?php
//search
include '../Model/config.php';

?>
<div class="container-scroller">
    <?php
            include ("testheader.php")
        ?>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
        
        <?php
            include ("menu.php")
        ?>
        
<div class="content-wrapper">    
        <div class="card">
            <div class="card-body">
              <h4 class="card-title">ยกเลิกการจองห้อง</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
                            <th>ห้องที่ถูกจอง</th>
                            <th>ชื่อผู้จอง</th>
                            <th>วันที่เริ่มต้น</th>
                            <th>วันที่สิ้นสุด</th>
                            <th>เวลาที่เริ่มจอง</th>
                            <th>เวลาสิ้นสุด</th>
                            <th>จอง ณ วันเวลาที่</th>
                            <th>ยกเลิกการจอง</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php   
if ($_SESSION["member_type"] == "professor") {
    $sql = "SELECT rq.classroom_id, rq.rq_classroom_id ,cr.classroom_name ,m.member_firstname , rq.rq_day_start ,rq.rq_day_end 
    ,rq.rq_time_start , rq.rq_time_end , rq.rq_date_now 
    FROM request_classroom AS rq 
    LEFT JOIN classroom AS cr ON (cr.classroom_id = rq.classroom_id)
    LEFT JOIN member AS m ON (rq.member_id = m.member_id)
    WHERE rq.member_id = '".$_SESSION['member_id']."' and rq.rq_approve_status = '0' ORDER BY rq.rq_date_now  DESC ";
} else if ($_SESSION["member_type"] == "student") {
    $sql = "SELECT rq.classroom_id, rq.rq_classroom_id ,cr.classroom_name ,m.member_firstname , rq.rq_day_start ,rq.rq_day_end 
    ,rq.rq_time_start , rq.rq_time_end , rq.rq_date_now 
    FROM request_classroom AS rq 
    LEFT JOIN classroom AS cr ON (cr.classroom_id = rq.classroom_id)
    LEFT JOIN member AS m ON (rq.member_id = m.member_id)
    WHERE rq.member_id = '".$_SESSION['member_id']."' and rq.rq_approve_status = '0' ORDER BY rq.rq_date_now  DESC ";
}else {
    echo "Error Session cancel-list";
}   
    
$query = mysqli_query($conn, $sql) or die("error");
while ($row = mysqli_fetch_array($query)) {
    ?>


            <tr>
            
            <td><label ><?php echo $row['classroom_name'] ?></label></td>
            <td><label ><?php echo $row['member_firstname'] ?></label></td>
            <td><label ><?php echo $row['rq_day_start'] ?></label></td>
            <td><label ><?php echo $row['rq_day_end'] ?></label></td>
            <td><label ><?php echo $row['rq_time_start'] ?></label></td>
            <td><label ><?php echo $row['rq_time_end'] ?></label></td>
            <td><label ><?php echo $row['rq_date_now'] ?></label></td>
            <td> <a href="../Controller/process_cancel_booking.php?cmd=delete&id=<?php echo $row['rq_classroom_id']; ?>" class="btn btn-danger">ยกเลิกการจอง</a> </td>
            
            
            </tr>
            <?php
}
?>
            </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
          </div>  
          
          
          
        </div>
        <?php
            include ("footer.php");
        ?>
      </div>
    </div>
  </div>




  


</body>
</html>