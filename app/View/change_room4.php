<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    
    <link rel="stylesheet" href="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/jquery-asColorPicker/dist/css/asColorPicker.min.css" />
    <link rel="stylesheet" href="../../public/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />


</head>
<body>
<?php

//search
include '../Model/config.php';

?>
<div class="container-scroller">
  
    <?php
            include ("testheader.php")
        ?>

    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
       
     
        <?php
            include ("menu.php")
        ?>






     
        <div class="content-wrapper">
        <?php
ini_set('display_errors', 1);
error_reporting(~0);
error_reporting(~E_NOTICE);

if (isset($_POST['day7c']) && isset($_POST['time_startc']) && isset($_POST['time_endc'])) {

    $day7c = $_POST['day7c'];
    $time_startc = $_POST['time_startc'];
    $time_endc = $_POST['time_endc'];

}
$schedulename = $_POST['schedulename'];
$day7 = $_POST['day7'];
$time_start = $_POST['time_start'];
$time_end = $_POST['time_end'];


if ($day7 == "1") {
    $newday = "วันจันทร์";
} else if ($day7 == "2") {
    $newday = "วันอังคาร";
} else if ($day7 == "3") {
    $newday = "วันพุธ";
} else if ($day7 == "4") {
    $newday = "วันพุฤหัสบดี";
} else if ($day7 == "5") {
    $newday = "วันศุกร์";
} else if ($day7 == "6") {
    $newday = "วันเสาร์";
} else if ($day7 == "7") {
    $newday = "วันอาทิตย์";
} else {

}

if ($day7c == "1") {
    $newdayc = "วันจันทร์";
} else if ($day7c == "2") {
    $newdayc = "วันอังคาร";
} else if ($day7c == "3") {
    $newdayc = "วันพุธ";
} else if ($day7c == "4") {
    $newdayc = "วันพุฤหัสบดี";
} else if ($day7c == "5") {
    $newdayc = "วันศุกร์";
} else if ($day7c == "6") {
    $newdayc = "วันเสาร์";
} else if ($day7c == "7") {
    $newdayc = "วันอาทิตย์";
} else {

}


?>
<div style="height:10px;"></div>
        <div class="card">
            <div class="card-body">
              <h4 class="card-title">ห้องเดิมของอาจารย์</h4>
              <div class="row">
                <div class="col-12 ">
                    <form name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']; ?>">
                    <div class="row justify-content-center">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เลือกวัน</label>
                          <div class="col-sm-9">
                          <select class="form-control " name="day7c" required>
                                                 <option value="<?php echo $newdayc ?>"><?php echo $newdayc ?></option>
                                                 <option value="1">วันจันทร์</option>
                                                 <option value="2">วันอังคาร</option>
                                                 <option value="3">วันพุธ</option>
                                                 <option value="4">วันพฤหัสบดี</option>
                                                 <option value="5">วันศุกร์</option>
                                                 <option value="6">วันเสาร์</option>
                                                 <option value="7">วันอาทิตย์</option>
                           </select> 
                        </div>
                      </div>
                      </div> 
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาเริ่มต้น</label>
                          <div class="input-group clockpicker col-sm-9">
                                <input class="form-control" name="time_startc" type="time" id="time_startc" value="<?php echo $time_startc ?>" />
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">                        
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">เวลาสิ้นสุด</label>
                          <div class="input-group clockpicker col-sm-9">
                              <input class="form-control" name="time_endc" type="time" id="time_endc"  value="<?php echo $time_endc ?>">
                                <span class="input-group-addon">
                                  <i class="mdi mdi-clock"></i>
                                </span>
                          </div>                          
                        </div>
                      </div> 
                    </div>          
                    <div class="row justify-content-center">                      
                      <div class="col-md-1 ">
                        <div class="form-group row">                          
                          <div class="col-sm-9">
                          <input type="submit" name="search" value="ค้นหา" class="btn btn-warning">
                          </div>
                        </div>
                      </div>                      
                    </div>   
            <input type="hidden" name="schedulename" value="<?php echo $schedulename ?>">
            <input type="hidden" name="day7" value="<?php echo $day7 ?>"> 
            <input type="hidden" name="time_start" value="<?php echo $time_start ?>">
            <input type="hidden" name="time_end" value="<?php echo $time_end ?>">                      
                    </form>
                    
                </div>
              </div>
            </div>
          </div> 

<div style="height:20px;"></div>

        <div class="card">
            <div class="card-body">
              <h4 class="card-title">เลือกห้องเดิมของอาจารย์</h4>
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            
                            <th>ห้องเรียน</th>
                            
                            <th>Action</th>
                        </tr>
                      </thead>
                      <input type="hidden" name="schedulename" value="<?php echo $schedulename ?>">
            <input type="hidden" name="day7" value="<?php echo $day7 ?>"> 
            <input type="hidden" name="time_start" value="<?php echo $time_start ?>">
            <input type="hidden" name="time_end" value="<?php echo $time_end ?>">

            <?php


$sql = "SELECT  sc.* FROM scheduleroom AS sc
WHERE (sc.schedule_day_start BETWEEN '" . $day7c . "' AND  '" . $day7c . "')
 AND(sc.schedule_day_end BETWEEN '" . $day7c . "' AND  '" . $day7c . "')
 AND(sc.schedule_time_start <='" . $time_endc . "' )
 AND( sc.schedule_time_end  >='" . $time_startc . "' )GROUP BY  sc.schedule_name ";
$query = mysqli_query($conn, $sql) or die("error" );

/*echo $day_of_weekc;
echo $day_of_weekc2;*/
while ($row = mysqli_fetch_array($query)) {
    ?>
        <tr>    
            <td>
            <a href="#change<?php echo $row['schedule_id']; ?>" data-toggle="modal" class="btn btn-outline-success" >
            <span class="glyphicon glyphicon-edit"></span> <?php echo $row['schedule_name'] ?></a> &nbsp;

            <form action="../Controller/process_class_schedule.php?cmd=changeroom&id" method="post">
                 <input type="hidden" name="schedulename" value="<?php echo $schedulename ?>">
                 <input type="hidden" name="day7c" value="<?php echo $day7c ?>"> 
                 <input type="hidden" name="time_startc" value="<?php echo $time_startc ?>">
                 <input type="hidden" name="time_endc" value="<?php echo $time_endc ?>">


            </form>
            <td>
                <a href="#change<?php echo $row['schedule_id']; ?>" data-toggle="modal" class="btn btn-outline-success" >
            <span class="glyphicon glyphicon-edit">เปลี่ยน</span></a>
            </td>
            
            </td>
        </tr>    

<!-- read -->
    <div class="modal fade" id="change<?php echo $row['schedule_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">

                 <div class="modal-header">
					<h4 align="center" class="modal-title" id="myModalLabel">ยืนยันรายการเปลี่ยนแปลงห้อง</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
				<?php
$sql2 = "SELECT sc.* from scheduleroom AS sc
                where sc.schedule_id='" . $row['schedule_id'] . "' ";
    $data = mysqli_query($conn, $sql2);
    $drow = mysqli_fetch_array($data);
    ?>

            <div class="d-flex justify-content-between">
                <div class="form-group form-control col-6">
                    <h5><strong><label for="">ก่อนเปลี่ยนแปลงห้อง</strong></label></h5>
                    <h6>ชื่อผู้ใช้งาน : <strong><?php echo $_SESSION["member_firstname"] ?></strong></h6>
                    <h6>วัน : <strong><?php echo $newdayc ?></strong></h6>
					<h6>ห้องเรียน : <strong><?php echo $drow['schedule_name'] ?></strong></h6>                  
                    <h6>เวลาเริ่มต้น : <strong><?php echo $time_startc ?></strong></h6>
                    <h6>เวลาสิ้นสุด : <strong><?php echo $time_endc ?></strong></h6>
                </div>&nbsp;
                <div class="form-group form-control col-6" >
                    <h5><strong><label for="">หลังเปลี่ยนแปลงห้อง</strong></label></h5>
                    <h6>ชื่อผู้ใช้งาน : <strong><?php echo $_SESSION["member_firstname"] ?></strong></h6>
                    <h6>วัน : <strong><?php echo $newday ?></strong></h6>
                    <h6>ห้องเรียน : <strong><?php echo $schedulename ?></strong></h6>            
                    <h6>เวลาเริ่มต้น : <strong><?php echo $time_start ?></strong></h6>
                    <h6>เวลาสิ้นสุด : <strong><?php echo $time_end ?></strong></h6>
					
                </div>
            </div>

                    <form action="../Controller/process_class_schedule.php?cmd=changeroom&id=<?php echo $drow['schedule_id']; ?>" method="post">


                        <input type="hidden" name="schedulename" value="<?php echo $schedulename ?>">
                        <input type="hidden" name="day7" value="<?php echo $day7 ?>"> 
                        <input type="hidden" name="time_start" value="<?php echo $time_start ?>">
                        <input type="hidden" name="time_end" value="<?php echo $time_end ?>">




				
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> ยกเลิก</button>
                    <button type="submit"  class="btn btn-warning"><span class="glyphicon glyphicon-check"></span> ตกลง</button>
                </div>
                    </form>
            </div>
        </div>
    </div>



            <?php
}
?>
            </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          
          
          
        </div>
   
        <?php
        include ("footer.php");

?>
       
      </div>     
    </div> 
  </div>

  <script src="../../public/node_modules/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
  <script src="../../public/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="../../public/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
  <script src="../../public/js/formpickers.js"></script>
  <script src="../../public/js/formpickers.js"></script>

</body>
</html>
