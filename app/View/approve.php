<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
 
     

</head>
<body>

<?php
//search
include '../Model/config.php';

?>

<div class="container-scroller">
  
  <?php
          include ("testheader.php")
      ?>

  <div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
     
   
      <?php
          include ("menu.php")
      ?>

   
      <div class="content-wrapper">
  
      <div class="card">
        
            <div class="card-body">
             <div class="pull-right">
                <a href="canapprove.php"  class="btn btn-outline-danger" > ยกเลิกการอนุมัติ</a>
              </div>
            <h4 class="card-title">อนุมัติการจอง</h4>
              
                
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>                            
                            <th>ห้องที่ถูกจอง</th>
                            <th>ชื่อผู้จอง</th>
                            <th>วันที่เริ่มต้น</th>
                            <th>วันที่สิ้นสุด</th>
                            <th>เวลาที่เริ่มจอง</th>
                            <th>เวลาสิ้นสุด</th>
                            <th>จอง ณ วันเวลาที่</th>
                            <th>อนุมัติ</th>
                            <th>ไม่อนุมัติ</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
$perpage = 5;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

$start = ($page - 1) * $perpage;
$sql = "SELECT  rq.* ,cr.classroom_name ,m.member_firstname  from request_classroom AS rq
LEFT JOIN classroom AS cr ON (rq.classroom_id = cr.classroom_id)
LEFT JOIN member AS m ON (rq.member_id = m.member_id)
WHERE rq.rq_approve_status = '0' and rq.rq_classroom_id NOT IN
(SELECT ap.rq_classroom_id  FROM approve AS ap ) limit $start , $perpage ";
$query = mysqli_query($conn, $sql) or die("error");
while ($row = mysqli_fetch_array($query)) {
    ?>


            <tr>





            <td><label ><?php echo $row['classroom_name'] ?></label></td>
            <td><label ><?php echo $row['member_firstname'] ?></label></td>
            <td><label ><?php echo $row['rq_day_start'] ?></label></td>
            <td><label ><?php echo $row['rq_day_end'] ?></label></td>
            <td><label ><?php echo $row['rq_time_start'] ?></label></td>
            <td><label ><?php echo $row['rq_time_end'] ?></label></td>
            <td><label ><?php echo $row['rq_date_now'] ?></label></td>
            <form action="../Controller/process_approve.php?cmd=approom" method="post">
            <td> <button type="submit" name="apprqroom" class="btn btn-success btn-sm" value="<?php echo $row['rq_classroom_id']; ?>" ><i class="fa fa-check"></i></button> </td>
            </form>

            <form action="../Controller/process_approve.php?cmd=noapproom&id" method="post">
            <td> <button type="submit" name="noapprqroom" class="btn btn-danger btn-sm " value="<?php echo $row['rq_classroom_id']; ?>"><i class="fa fa-times"></i> </button></td>
            </form>

            </tr>
            <?php
}
?>
            </tbody>
                    </table>                    
                  </div>
                </div>
              </div>
            </div>
          </div>  
        
        




      
        
      </div>
 
     <?php
        include ("footer.php");

?>
     
    </div>
   
  </div>

</div>







</body>
</html>