<?php

$upload_dir = '../../public/images/';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Victory Admin</title>
 



  
  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                <div class="profile-image">
                  <img src="<?php echo $upload_dir .  $_SESSION["member_profile_pic"] ?>" alt="image" />
                  
                  <span class="online-status online"></span> 
                </div>
                <div class="profile-name">
                  <p class="name">
                  <?php echo $_SESSION["member_firstname"] ?>
                  </p>
                  <p class="designation">
                    <?php
                        if ($_SESSION["member_type"] == "admin") {
                          echo "ผู้ดูแลระบบ";
                        } else if ($_SESSION["member_type"] == "professor") {
                          echo "อาจารย์";
                        } else if ($_SESSION["member_type"] == "student") {
                          echo "นักศึกษา";
                        } else {
                          echo "Error Session type";
                        }
                    ?>
                    
                  </p>
                  <p class="card-description">    
                    <a  href="../Model/process.php?cmd=logout"><span class='menu-title'>ออกจากระบบ</span></a>
                  </p>
                </div>
              </div>
            </li>
               <?php
                    if ($_SESSION["member_type"] == "admin") {
                    echo "
            <li class='nav-item'>
              <a class='nav-link' data-toggle='collapse' href='#page-layouts' aria-expanded='false' aria-controls='page-layouts'>
                <i class='fa fa-book'></i>
                <span class='menu-title'>&nbsp; ข้อมูลพื้นฐาน</span>
               
              </a>
              <div class='collapse' id='page-layouts'>
                <ul class='nav flex-column sub-menu'>
                                        <li class='nav-item'><a class='nav-link' href='member.php'> ข้อมูลผู้ใช้งาน</a></li>
                                        <li class='nav-item'><a class='nav-link' href='faculty.php'> คณะ</a></li>
                                        <li class='nav-item'><a class='nav-link' href='branch.php'> สาขา</a></li>
                                        <li class='nav-item'><a class='nav-link' href='building.php'> ข้อมูลอาคารสถานที่</a></li>
                                        <li class='nav-item'><a class='nav-link' href='addclassroom.php'> เพิ่มตารางห้องเรียน</a></li>
                                        <li class='nav-item'><a class='nav-link' href='classroom.php'> ข้อมูลห้องเรียน</a></li>
                                        <li class='nav-item'><a class='nav-link' href='equipment.php'> อุปกรณ์</a></li>
                                        <li class='nav-item'><a class='nav-link' href='table.php'> ลักษณะโต๊ะ</a></li>
                                        <li class='nav-item'><a class='nav-link' href='chair.php'> ลักษณะเก้าอี้</a></li>

                                       
                </ul>
                
              </div>
            </li>
            <li class='nav-item '>
              <a class='nav-link'  href='approve.php'>
                <i class=' fa fa-thumbs-o-up'></i>
                <span class='menu-title'>&nbsp; อนุมัติการจอง</span>                
              </a>            
            </li>
            <li class='nav-item '>
              <a class='nav-link'  href='class_schedule.php'>
                <i class=' fa fa-table'></i>
                <span class='menu-title'>&nbsp; จองห้องเรียน</span>                
              </a>            
            </li>
            <li class='nav-item '>
              <a class='nav-link'  href='change_room1.php'>
                <i class=' fa fa-exchange'></i>
                <span class='menu-title'>&nbsp; เปลี่ยนแปลงห้องเรียน</span>                
              </a>            
            </li>
            
            <li class='nav-item '>
              <a class='nav-link'  href='bookinglist.php'>
                <i class='fa fa-list'></i>
                <span class='menu-title'>&nbsp; รายการจอง</span>                
              </a>            
            </li> " ;
                  } else if ($_SESSION["member_type"] == "professor") {
                    echo "
            <li class='nav-item '>
              <a class='nav-link'  href='edit_profile.php'>
                <i class=' fa fa-edit'></i>
                <span class='menu-title'>&nbsp; แก้ไขข้อมูลผู้ใช้</span>                
              </a>            
            </li>
            <li class='nav-item '>
              <a class='nav-link'  href='class_schedule.php'>
                <i class=' fa fa-table'></i>
                <span class='menu-title'>&nbsp; จองห้องเรียน</span>                
              </a>            
            </li>
            <li class='nav-item '>
              <a class='nav-link'  href='bookinglist.php'>
                <i class=' fa fa-list'></i>
                <span class='menu-title'>&nbsp; รายการจอง</span>                
              </a>            
            </li>
            
            <li class='nav-item '>
              <a class='nav-link'  href='cancel_booking.php'>
                <i class='fa fa-ban'></i>
                <span class='menu-title'>&nbsp; ยกเลิกการจอง</span>                
              </a>            
            </li>  ";
                  } else if ($_SESSION["member_type"] == "student") {
                    echo "
            <li class='nav-item '>
              <a class='nav-link'  href='edit_profile.php'>
                <i class=' fa fa-edit'></i>
                <span class='menu-title'>&nbsp; แก้ไขข้อมูลผู้ใช้</span>                
              </a>            
            </li>
            <li class='nav-item '>
              <a class='nav-link'  href='class_schedule.php'>
                <i class=' fa fa-table'></i>
                <span class='menu-title'>&nbsp; จองห้องเรียน</span>                
              </a>            
            </li>
            <li class='nav-item '>
              <a class='nav-link'  href='bookinglist.php'>
                <i class=' fa fa-list'></i>
                <span class='menu-title'>&nbsp; รายการจอง</span>                
              </a>            
            </li>
            
            <li class='nav-item '>
              <a class='nav-link'  href='cancel_booking.php'>
                <i class='fa fa-ban'></i>
                <span class='menu-title'>&nbsp; ยกเลิกการจอง</span>                
              </a>            
            </li> ";
                  } else {
                      echo "Error Session Menu";
                  }

            ?>
            
             

          </ul>
        </nav>



  <script src="../../public/node_modules/jquery/dist/jquery.min.js"></script>
  <script src="../../public/node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="../../public/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../public/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <script src="../../public/node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
  <script src="../../public/node_modules/chart.js/dist/Chart.min.js"></script>
  <script src="../../public/node_modules/raphael/raphael.min.js"></script>
  <script src="../../public/node_modules/morris.js/morris.min.js"></script>
  <script src="../../public/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
  <script src="../../public/js/off-canvas.js"></script>
  <script src="../../public/js/hoverable-collapse.js"></script>
  <script src="../../public/js/misc.js"></script>
  <script src="../../public/js/settings.js"></script>
  <script src="../../public/js/todolist.js"></script>
  <script src="../../public/js/dashboard.js"></script>
  
  <script src="../../public/node_modules/datatables.net/js/jquery.dataTables.js"></script>
  <script src="../../public/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
  <script src="../../public/js/data-table.js"></script>

  
  <script src="../../public/node_modules/icheck/icheck.min.js"></script>
  <script src="../../public/node_modules/typeahead.js/dist/typeahead.bundle.min.js"></script>
  <script src="../../public/node_modules/select2/dist/js/select2.min.js"></script>
  <script src="../../public/js/file-upload.js"></script>
  <script src="../../public/js/iCheck.js"></script>
  <script src="../../public/js/typeahead.js"></script>
  <script src="../../public/js/select2.js"></script>
   
  
  
  
  
	
  <script src="../../public/node_modules/dropify/dist/js/dropify.min.js"></script> 
  <script src="../../public/js/dropify.js"></script>


</body>

</html>
