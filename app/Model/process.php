<?php

include 'config.php';
$upload_dir = '../../public/images/';
session_start();

//login
if ($_GET['cmd'] == "login") {  

    
    $sql = "SELECT * FROM member WHERE member_username = '" . mysqli_real_escape_string($conn, $_POST['username']) . "'
	and member_password = '" . mysqli_real_escape_string($conn, $_POST['password']) . "'";

$query = mysqli_query($conn, $sql);
$result = mysqli_fetch_array($query, MYSQLI_ASSOC);

    if (!$result) {

        echo "<script>
        alert(' รหัสผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง!!');
        window.location = '../login.html';
        </script>" . mysqli_error($conn);
        
        
    } else {
        $_SESSION["member_id"] = $result["member_id"];
        $_SESSION["member_type"] = $result["member_type"];
        $_SESSION["member_firstname"] = $result["member_firstname"];
        $_SESSION["member_profile_pic"] = $result["member_profile_pic"];

        session_write_close();

        if ( $_SESSION["member_type"] == "admin") {
    
            header('location:../View/approve.php');
            
           
        } else if ( $_SESSION["member_type"] == "professor") {
            
            header('location:../View/class_schedule.php');
           
           
        } else if ( $_SESSION["member_type"] == "student") {
            header('location:../View/class_schedule.php');
           
        } else {
            echo "Error Session content";
        }

        /*
        if ($_SESSION["member_type"] == "admin") {
            header("location:../View/admin/class_schedule.php");
        } else if ($_SESSION["member_type"] == "professor") {
            header("location:../View/professor/class_schedule.php");
        } else  {
            header("location:../View/student/class_schedule.php");
        }       */
     
    }

    mysqli_close($conn);
}

//register
if ($_GET['cmd'] == "register") {

    $faculty = $_POST['faculty'];
    $branch = $_POST['branch'];
    $username = $_POST['username'];
    $pass = $_POST['password'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $tel = $_POST['tel'];
    $member_type = $_POST['member_type'];

    $imgName = $_FILES['image']['name'];
    $imgTmp = $_FILES['image']['tmp_name'];
    $imgSize = $_FILES['image']['size'];

    //echo $imgName . "<br>";
    //echo $imgTmp . "<br>";
    //echo $imgSize . "<br>";

    $imgExt = strtolower(pathinfo($imgName, PATHINFO_EXTENSION));

    $allowExt = array('jpeg', 'jpg', 'png', 'gif');

    $userPic = time() . '_' . rand(1000, 9999) . '.' . $imgExt;

    if (in_array($imgExt, $allowExt)) {

        if ($imgSize < 5000000) {
            move_uploaded_file($imgTmp, $upload_dir . $userPic);
        } else {
            $errorMsg = 'Image too large';
        }
    } else {
        $errorMsg = 'Please select a valid image';
    } 

    $strSQL = "INSERT INTO member (faculty_id, branch_id, member_username, member_password ,member_profile_pic ,member_firstname 
    ,member_lastname ,member_tel ,member_type) VALUES
     ( '$faculty', '$branch', '$username', '$pass','$userPic', '$firstname' , '$lastname' , $tel ,'$member_type'  )";

    $objQuery = mysqli_query($conn, $strSQL);
    if ($objQuery) {
        echo "<script>
        alert('สมัครสมาชิกสำเร็จ');
        window.location = '../login.html';
        </script>" . mysqli_error($conn);
    } else {
        echo "Error Save "  . mysqli_error($conn);
    }
    mysqli_close($conn);
}
//logout
if ($_GET['cmd'] == "logout") {

   
    session_destroy();

    header("location:../home.php");
}
