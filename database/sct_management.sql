-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2019 at 11:55 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sct_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `approve`
--

CREATE TABLE `approve` (
  `approve_id` int(5) NOT NULL,
  `rq_classroom_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `approve`
--

INSERT INTO `approve` (`approve_id`, `rq_classroom_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(2) NOT NULL,
  `faculty_id` int(2) NOT NULL,
  `branch_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `faculty_id`, `branch_name`) VALUES
(1, 1, 'วิทยาการคอมพิวเตอร์'),
(5, 0, 'บัญชี'),
(6, 0, 'พืช'),
(7, 0, 'ประมง');

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `building_id` int(2) NOT NULL,
  `building_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`building_id`, `building_name`) VALUES
(1, 'LB5'),
(2, 'SC'),
(3, 'LB3'),
(4, 'EN');

-- --------------------------------------------------------

--
-- Table structure for table `classroom`
--

CREATE TABLE `classroom` (
  `classroom_id` int(2) NOT NULL,
  `building_id` int(2) DEFAULT NULL,
  `equipment_id` int(2) DEFAULT NULL,
  `equipment_id1` int(2) DEFAULT NULL,
  `equipment_id2` int(2) DEFAULT NULL,
  `equipment_id3` int(2) DEFAULT NULL,
  `equipment_id4` int(2) DEFAULT NULL,
  `equipment_id5` int(2) DEFAULT NULL,
  `equipment_name` varchar(255) DEFAULT NULL,
  `furniture_t_id` int(2) DEFAULT NULL,
  `furniture_c_id` int(2) DEFAULT NULL,
  `classroom_name` varchar(50) NOT NULL,
  `classroom_num_seat` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classroom`
--

INSERT INTO `classroom` (`classroom_id`, `building_id`, `equipment_id`, `equipment_id1`, `equipment_id2`, `equipment_id3`, `equipment_id4`, `equipment_id5`, `equipment_name`, `furniture_t_id`, `furniture_c_id`, `classroom_name`, `classroom_num_seat`) VALUES
(103, 2, 9, 5, 8, NULL, NULL, NULL, 'แอร์,ไมค์', 1, 5, 'SC1065', 30),
(105, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'ลำโพง,โปรเจคเตอร์,วิชวลไลเซอร์', 1, 5, 'SC1068', 50),
(109, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'วิชวลไลเซอร์', 1, 5, 'SC1068', 20),
(110, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'โปรเจคเตอร์,วิชวลไลเซอร์', 2, 4, 'SC1005', 40),
(111, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'ลำโพง,โปรเจคเตอร์', 2, 4, 'SC1065', 30),
(112, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'โปรเจคเตอร์', 1, 4, 'LB5103', 100),
(113, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'แอร์,ไมค์,วิชวลไลเซอร์', 1, 4, 'LB3501', 20),
(115, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'ไมค์,ลำโพง', 1, 5, 'SC1061', 30),
(116, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ไมค์,ลำโพง,วิชวลไลเซอร์', 1, 4, 'LB5404', 40),
(117, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'ไมค์,ลำโพง', 1, 4, 'SC1078', 20),
(118, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'โปรเจคเตอร์,วิชวลไลเซอร์', 1, 5, 'sc1111', 40),
(119, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'โปรเจคเตอร์,วิชวลไลเซอร์', 1, 5, 'sc1222', 30),
(120, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'ลำโพง,โปรเจคเตอร์', 1, 4, 'LB3311', 10),
(121, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'ลำโพง,โปรเจคเตอร์', 1, 5, 'LB3311', 10),
(122, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'ลำโพง,โปรเจคเตอร์', 1, 5, 'LB3311', 10),
(123, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'ลำโพง,โปรเจคเตอร์', 1, 5, 'LB3311', 10),
(124, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'ลำโพง,โปรเจคเตอร์', 1, 5, 'LB3311', 10);

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `equipment_id` int(2) NOT NULL,
  `equipment_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`equipment_id`, `equipment_name`) VALUES
(5, 'แอร์'),
(6, 'ไมค์'),
(7, 'ลำโพง'),
(8, 'โปรเจคเตอร์'),
(9, 'วิชวลไลเซอร์');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `faculty_id` int(2) NOT NULL,
  `faculty_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`faculty_id`, `faculty_name`) VALUES
(6, 'เทคโนโลยีอุตสาหกรรมการเกษตร'),
(8, 'เทคโนโลยีสังคม');

-- --------------------------------------------------------

--
-- Table structure for table `furniture_c`
--

CREATE TABLE `furniture_c` (
  `furniture_c_id` int(2) NOT NULL,
  `furniture_c_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `furniture_c`
--

INSERT INTO `furniture_c` (`furniture_c_id`, `furniture_c_name`) VALUES
(4, 'เก้าอี้พลาสติกสีน้ำเงิน'),
(5, 'เก้าอี้ขาว');

-- --------------------------------------------------------

--
-- Table structure for table `furniture_t`
--

CREATE TABLE `furniture_t` (
  `furniture_t_id` int(2) NOT NULL,
  `furniture_t_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `furniture_t`
--

INSERT INTO `furniture_t` (`furniture_t_id`, `furniture_t_name`) VALUES
(1, 'โต๊ะขาวยาว'),
(2, 'โต๊ะเล็ก');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(5) NOT NULL,
  `faculty_id` int(2) NOT NULL,
  `branch_id` int(2) NOT NULL,
  `member_username` varchar(15) NOT NULL,
  `member_password` varchar(50) NOT NULL,
  `member_profile_pic` varchar(255) NOT NULL,
  `member_firstname` varchar(100) NOT NULL,
  `member_lastname` varchar(100) NOT NULL,
  `member_tel` varchar(15) NOT NULL,
  `member_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `faculty_id`, `branch_id`, `member_username`, `member_password`, `member_profile_pic`, `member_firstname`, `member_lastname`, `member_tel`, `member_type`) VALUES
(27, 7, 1, 'admin3', '1472', '1555593218_7429.png', 'yvg', 'cu', '5153158', 'admin'),
(32, 8, 1, 'professor', '1472', '1550997158_8103.jpg', 'professor', '1472', 'ddddd', 'professor'),
(34, 8, 1, '035830271015-9', '1234', '1555345906_2144.jpg', 'ปณัย', 'ขัติยะนนท์', '65846068043', 'student'),
(35, 8, 1, '1235', '', '1556518101_5062.png', 'ฟิวส์', 'ครับ', '486', 'ประเภทสมาชิก'),
(36, 8, 1, '147256', '147256', '1556518839_2146.png', 'ปณัย55', 'fdsa', '161535', 'student');

-- --------------------------------------------------------

--
-- Table structure for table `request_classroom`
--

CREATE TABLE `request_classroom` (
  `rq_classroom_id` int(10) NOT NULL,
  `member_id` int(5) NOT NULL,
  `classroom_id` int(10) NOT NULL,
  `rq_day_start` date NOT NULL,
  `rq_day_end` date NOT NULL,
  `rq_time_start` time NOT NULL,
  `rq_time_end` time NOT NULL,
  `rq_date_now` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rq_approve_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request_classroom`
--

INSERT INTO `request_classroom` (`rq_classroom_id`, `member_id`, `classroom_id`, `rq_day_start`, `rq_day_end`, `rq_time_start`, `rq_time_end`, `rq_date_now`, `rq_approve_status`) VALUES
(1, 27, 116, '2019-04-29', '2019-04-29', '09:00:00', '11:00:00', '2019-04-29 06:13:20', 1),
(2, 32, 112, '2019-05-01', '2019-05-01', '09:00:00', '11:00:00', '2019-04-29 06:16:59', 1),
(3, 36, 116, '2019-05-02', '2019-05-02', '09:00:00', '11:00:00', '2019-04-29 06:24:33', 3);

-- --------------------------------------------------------

--
-- Table structure for table `scheduleroom`
--

CREATE TABLE `scheduleroom` (
  `schedule_id` int(5) NOT NULL,
  `schedule_name` varchar(70) NOT NULL,
  `schedule_day_start` enum('1','2','3','4','5','6','7') NOT NULL,
  `schedule_day_end` enum('1','2','3','4','5','6','7') NOT NULL,
  `schedule_time_start` time NOT NULL,
  `schedule_time_end` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scheduleroom`
--

INSERT INTO `scheduleroom` (`schedule_id`, `schedule_name`, `schedule_day_start`, `schedule_day_end`, `schedule_time_start`, `schedule_time_end`) VALUES
(53, 'sc1063', '1', '1', '09:00:00', '11:00:00'),
(54, 'LB3303', '2', '2', '09:00:00', '11:00:00'),
(55, 'sc1066', '3', '3', '09:00:00', '11:00:00'),
(56, 'LB3501', '1', '1', '09:00:00', '11:00:00'),
(57, 'sc1061', '5', '5', '09:00:00', '11:00:00'),
(58, 'sc1062', '6', '6', '13:00:00', '15:00:00'),
(59, 'sc1061', '7', '7', '09:00:00', '11:00:00'),
(60, 'LB5104', '2', '2', '09:00:00', '11:00:00'),
(61, 'sc1071', '6', '6', '13:00:00', '15:00:00'),
(62, 'sc1072', '6', '6', '13:00:00', '15:00:00'),
(63, 'sc1073', '6', '6', '13:00:00', '15:00:00'),
(64, 'sc1074', '6', '6', '09:00:00', '11:00:00'),
(65, 'sc1075', '5', '5', '09:00:00', '11:00:00'),
(66, 'sc1076', '5', '5', '09:00:00', '11:00:00'),
(67, 'sc1077', '5', '5', '09:00:00', '11:00:00'),
(68, 'LB5103', '1', '1', '09:00:00', '11:00:00'),
(69, 'LB3303', '3', '3', '09:00:00', '11:00:00'),
(70, 'LB3303', '1', '1', '09:00:00', '11:00:00'),
(71, 'LB5103', '2', '2', '09:00:00', '11:00:00'),
(72, 'sc1082', '2', '2', '13:00:00', '15:00:00'),
(73, 'sc1090', '1', '1', '09:00:00', '11:00:00'),
(74, 'sc1091', '2', '2', '13:00:00', '15:00:00'),
(75, 'sc1092', '3', '3', '09:00:00', '11:00:00'),
(76, 'sc1093', '4', '4', '09:00:00', '11:00:00'),
(77, 'sc1094', '5', '5', '09:00:00', '11:00:00'),
(78, 'sc1095', '6', '6', '13:00:00', '15:00:00'),
(79, 'sc1096', '7', '7', '09:00:00', '11:00:00'),
(80, 'LB3311', '3', '3', '09:00:00', '11:00:00'),
(81, 'sc1098', '6', '6', '13:00:00', '15:00:00'),
(82, 'sc1099', '6', '6', '13:00:00', '15:00:00'),
(83, 'sc1100', '6', '6', '13:00:00', '15:00:00'),
(84, 'sc1101', '6', '6', '09:00:00', '11:00:00'),
(85, 'sc1102', '5', '5', '09:00:00', '11:00:00'),
(86, 'sc1103', '5', '5', '09:00:00', '11:00:00'),
(87, 'sc1104', '5', '5', '09:00:00', '11:00:00'),
(88, 'sc1105', '4', '4', '09:00:00', '11:00:00'),
(89, 'LB3311', '2', '2', '09:00:00', '11:00:00'),
(90, 'LB3501', '2', '2', '09:00:00', '11:00:00'),
(91, 'sc1108', '4', '4', '13:00:00', '15:00:00'),
(92, 'sc1109', '2', '2', '13:00:00', '15:00:00'),
(93, 'sc1110', '1', '1', '09:00:00', '11:00:00'),
(94, 'sc1111', '2', '2', '13:00:00', '15:00:00'),
(95, 'sc1112', '3', '3', '09:00:00', '11:00:00'),
(96, 'sc1113', '4', '4', '09:00:00', '11:00:00'),
(97, 'sc1114', '5', '5', '09:00:00', '11:00:00'),
(98, 'sc1115', '6', '6', '13:00:00', '15:00:00'),
(99, 'sc1116', '7', '7', '09:00:00', '11:00:00'),
(100, 'sc1117', '4', '4', '13:00:00', '15:00:00'),
(101, 'sc1118', '6', '6', '13:00:00', '15:00:00'),
(102, 'sc1119', '6', '6', '13:00:00', '15:00:00'),
(103, 'sc1120', '6', '6', '13:00:00', '15:00:00'),
(104, 'sc1121', '6', '6', '09:00:00', '11:00:00'),
(105, 'sc1122', '5', '5', '09:00:00', '11:00:00'),
(106, 'sc1123', '5', '5', '09:00:00', '11:00:00'),
(107, 'sc1124', '5', '5', '09:00:00', '11:00:00'),
(108, 'sc1125', '4', '4', '09:00:00', '11:00:00'),
(109, 'sc1126', '4', '4', '13:00:00', '15:00:00'),
(110, 'sc1127', '4', '4', '13:00:00', '15:00:00'),
(111, 'sc1128', '4', '4', '13:00:00', '15:00:00'),
(112, 'sc1129', '2', '2', '13:00:00', '15:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `approve`
--
ALTER TABLE `approve`
  ADD PRIMARY KEY (`approve_id`),
  ADD KEY `rq_classroom_id` (`rq_classroom_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `branch_ibfk_1` (`faculty_id`);

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`building_id`);

--
-- Indexes for table `classroom`
--
ALTER TABLE `classroom`
  ADD PRIMARY KEY (`classroom_id`),
  ADD KEY `classroom_ibfk_2` (`furniture_c_id`),
  ADD KEY `classroom_ibfk_3` (`furniture_t_id`),
  ADD KEY `classroom_ibfk_4` (`equipment_id`),
  ADD KEY `building_id` (`building_id`),
  ADD KEY `equipment_id1` (`equipment_id1`),
  ADD KEY `equipment_id2` (`equipment_id2`),
  ADD KEY `equipment_id3` (`equipment_id3`),
  ADD KEY `equipment_id4` (`equipment_id4`),
  ADD KEY `equipment_id5` (`equipment_id5`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`equipment_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `furniture_c`
--
ALTER TABLE `furniture_c`
  ADD PRIMARY KEY (`furniture_c_id`);

--
-- Indexes for table `furniture_t`
--
ALTER TABLE `furniture_t`
  ADD PRIMARY KEY (`furniture_t_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Indexes for table `request_classroom`
--
ALTER TABLE `request_classroom`
  ADD PRIMARY KEY (`rq_classroom_id`),
  ADD KEY `request_classroom_ibfk_2` (`classroom_id`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `scheduleroom`
--
ALTER TABLE `scheduleroom`
  ADD PRIMARY KEY (`schedule_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `approve`
--
ALTER TABLE `approve`
  MODIFY `approve_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `building_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `classroom`
--
ALTER TABLE `classroom`
  MODIFY `classroom_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `equipment_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `faculty_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `furniture_c`
--
ALTER TABLE `furniture_c`
  MODIFY `furniture_c_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `furniture_t`
--
ALTER TABLE `furniture_t`
  MODIFY `furniture_t_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `request_classroom`
--
ALTER TABLE `request_classroom`
  MODIFY `rq_classroom_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scheduleroom`
--
ALTER TABLE `scheduleroom`
  MODIFY `schedule_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `approve`
--
ALTER TABLE `approve`
  ADD CONSTRAINT `approve_ibfk_1` FOREIGN KEY (`rq_classroom_id`) REFERENCES `request_classroom` (`rq_classroom_id`);

--
-- Constraints for table `classroom`
--
ALTER TABLE `classroom`
  ADD CONSTRAINT `classroom_ibfk_10` FOREIGN KEY (`equipment_id5`) REFERENCES `equipment` (`equipment_id`),
  ADD CONSTRAINT `classroom_ibfk_2` FOREIGN KEY (`furniture_c_id`) REFERENCES `furniture_c` (`furniture_c_id`),
  ADD CONSTRAINT `classroom_ibfk_3` FOREIGN KEY (`furniture_t_id`) REFERENCES `furniture_t` (`furniture_t_id`),
  ADD CONSTRAINT `classroom_ibfk_4` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`equipment_id`),
  ADD CONSTRAINT `classroom_ibfk_5` FOREIGN KEY (`building_id`) REFERENCES `building` (`building_id`),
  ADD CONSTRAINT `classroom_ibfk_6` FOREIGN KEY (`equipment_id1`) REFERENCES `equipment` (`equipment_id`),
  ADD CONSTRAINT `classroom_ibfk_7` FOREIGN KEY (`equipment_id2`) REFERENCES `equipment` (`equipment_id`),
  ADD CONSTRAINT `classroom_ibfk_8` FOREIGN KEY (`equipment_id3`) REFERENCES `equipment` (`equipment_id`),
  ADD CONSTRAINT `classroom_ibfk_9` FOREIGN KEY (`equipment_id4`) REFERENCES `equipment` (`equipment_id`);

--
-- Constraints for table `request_classroom`
--
ALTER TABLE `request_classroom`
  ADD CONSTRAINT `request_classroom_ibfk_2` FOREIGN KEY (`classroom_id`) REFERENCES `classroom` (`classroom_id`),
  ADD CONSTRAINT `request_classroom_ibfk_3` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
