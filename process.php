<?php

include '../../config.php';
session_start();

//login
if ($_GET['cmd'] == "login") {

    if (empty($_POST["m_username"]) || empty($_POST["m_password"])) {
        echo '<script>alert("Both Fields are required")</script>';
    } else {
        $m_username = mysqli_real_escape_string($conn, $_POST["m_username"]);
        $m_password = mysqli_real_escape_string($conn, $_POST["m_password"]);
        $query = "SELECT * FROM members WHERE m_username = '$m_username'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_array($result)) {
                if (password_verify($m_password, $row["m_password"])) {
                    //return true;
                    $_SESSION["m_username"] = $m_username;
                    $_SESSION["m_id"] = $row["m_id"];
                    $_SESSION["m_type"] = $row["m_type"];
                    $_SESSION["m_firstname"] = $row["m_firstname"];
                    session_write_close();

                    if ($row["m_type"] == "1") {
                        header("location:../admin/ma_main.php");
                    } else {
                        header("location:lesson.php");
                    }
                } else {
                    //return false;
                    echo '<script>alert("Wrong User Details")</script>';
                    $msg = "รหัสผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง";
                    header("location:login.php?error=$msg");
                }
            }
        } else {
            echo '<script>alert("Wrong User Details")</script>';
            $msg = "รหัสผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง";
            header("location:login.php?error=$msg");
        }
    }

    /* $strSQL = "SELECT * FROM member WHERE m_username = '" . mysqli_real_escape_string($conn, $_POST['m_username']) . "'
and m_password = '" . mysqli_real_escape_string($conn, md5($_POST['m_password'])) . "'";

$objQuery = mysqli_query($conn, $strSQL);
$objResult = mysqli_fetch_array($objQuery, MYSQLI_ASSOC);

if (!$objResult) {
echo "Username and Password Incorrect!";
header("location:login2.php");
} else {
$_SESSION["m_id"] = $objResult["m_id"];
$_SESSION["m_type"] = $objResult["m_type"];
$_SESSION["m_firstname"] = $objResult["m_firstname"];

session_write_close();

if ($objResult["m_type"] == "1") {
header("location:../admin/ma_lesson.php");
} else {
header("location:lesson.php");
}
}

mysqli_close($conn);*/
}

//register
if ($_GET['cmd'] == "register") {

    $m_firstname = $_POST['m_firstname'];
    $m_lastname = $_POST['m_lastname'];
    $m_gender = $_POST['m_gender'];
    $m_forget_ans = $_POST['m_forget_ans'];
    $m_ans = $_POST['m_ans'];
    $m_birth_date = $_POST['m_birth_date'];
    $m_email = $_POST['m_email'];

    $m_username = mysqli_real_escape_string($conn, $_POST["m_username"]);
    $m_password = mysqli_real_escape_string($conn, $_POST["m_password"]);
    $m_password = password_hash($m_password, PASSWORD_DEFAULT);
    //ตรวจสอบว่ามี user นี้อยู่แล้วหรือไม่
    $sql = "SELECT m_username FROM members WHERE m_username =  '$m_username'";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) >= 1) {

        header('location: register.php?error=' . urldecode('Username นี้ไม่สามารถใช้ได้'));

    } else {
        $query = "INSERT INTO members (m_username, m_password, m_firstname, m_lastname, m_type, m_gender, m_forget_ans ,m_ans ,m_birth_date, m_email, m_str_date) values('$m_username', '$m_password', '$m_firstname', '$m_lastname', '0', '$m_gender' ,'$m_forget_ans' ,'$m_ans' ,'$m_birth_date', '$m_email', NOW())";

        if (mysqli_query($conn, $query)) {
            echo "<script> alert('สมัครสมาชิกสำเร็จ');
            window.location.href = 'login.php';
                   </script>";
        } else {
            die("Connection failed555: " . mysqli_connect_error());
        }
    }

    /*$m_type = "0";
$m_username = $_POST["m_username"];

$strSQL = "INSERT INTO member (m_username, m_password, m_firstname, m_lastname, m_type, m_str_date, m_forget_ans, m_ans ) VALUES ('" . $_POST["m_username"] . "',
'" . md5($_POST["m_password"]) . "','" . $_POST["m_firstname"] . "','" . $_POST["m_lastname"] . "' ,'" . $m_type . "', '" . date("Y-m-d ") . "','" . $_POST["m_forget_ans"] . "' , '" . $_POST["m_ans"] . "' )";

//ตรวจสอบว่ามี user นี้อยู่แล้วหรือไม่
$sql = "SELECT m_username FROM member WHERE m_username =  '$m_username'";
$result = mysqli_query($conn, $sql);
$numbar_of_results = mysqli_num_rows($result);

if ($numbar_of_results > 0) {
header('location: register.php?error=' . urldecode('Username นี้ไม่สามารถใช้ได้'));
} else {
$objQuery = mysqli_query($conn, $strSQL);
if ($objQuery) {
header("location:register.php");
} else {
echo "Error Save [" . $strSQL . "]";

}
mysqli_close($conn);
}*/
}
//logout
if ($_GET['cmd'] == "logout") {

    session_start();
    session_destroy();

    header("location:home.php");
}

//edit
if ($_GET['cmd'] == "edit") {

    $m_id = $_GET['m_id'];

    $m_firstname = $_POST['m_firstname'];
    $m_lastname = $_POST['m_lastname'];

    $m_gender = $_POST['m_gender'];
    $m_forget_ans = $_POST['m_forget_ans'];
    $m_ans = $_POST['m_ans'];
    $m_birth_date = $_POST['m_birth_date'];
    $m_email = $_POST['m_email'];

    if (!isset($errorMsg)) {
        $sql = "update members set m_firstname='$m_firstname', m_lastname='$m_lastname' ,m_gender='$m_gender' ,m_forget_ans='$m_forget_ans', m_ans='$m_ans', m_birth_date='$m_birth_date', m_email='$m_email'   where m_id='$m_id'  ";

        if (mysqli_query($conn, $sql)) {
            echo "<script> alert('แก้ไขข้อมูลสำเร็จ');
            window.location.href = 'lesson.php';
                   </script>";
        } else {
            echo "Error updating record: " . mysqli_error($conn);
        }
    }

}

if ($_GET['cmd'] == "edit_pw") {

    $m_user = $_GET['user'];

    $m_password = mysqli_real_escape_string($conn, $_POST["m_password"]);
    $m_password = password_hash($m_password, PASSWORD_DEFAULT);

    if (!isset($errorMsg)) {
        $sql = "update members set m_password='$m_password'  where m_username = '$m_user'";
        if (mysqli_query($conn, $sql)) {
            echo "<script> alert('แก้ไขข้อมูลสำเร็จ');
            window.location.href = 'login.php';
                   </script>";
        } else {
            echo "Error updating record: " . mysqli_error($conn);
        }
    }
}
